/* Include files */

#include "Building_Modeling_R2020a_sfun.h"
#include "c3_Building_Modeling_R2020a.h"
#include <string.h>
#include "mwmathutil.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c(S);
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

/* Forward Declarations */

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static emlrtMCInfo c3_emlrtMCI = { 158,/* lineNo */
  17,                                  /* colNo */
  "eml_rand_mt19937ar",                /* fName */
  "C:\\Program Files\\MATLAB\\R2021a\\toolbox\\eml\\lib\\matlab\\randfun\\eml_rand_mt19937ar.m"/* pName */
};

static emlrtMCInfo c3_b_emlrtMCI = { 46,/* lineNo */
  23,                                  /* colNo */
  "sumprod",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2021a\\toolbox\\eml\\lib\\matlab\\datafun\\private\\sumprod.m"/* pName */
};

static emlrtMCInfo c3_c_emlrtMCI = { 88,/* lineNo */
  9,                                   /* colNo */
  "eml_int_forloop_overflow_check",    /* fName */
  "C:\\Program Files\\MATLAB\\R2021a\\toolbox\\eml\\lib\\matlab\\eml\\eml_int_forloop_overflow_check.m"/* pName */
};

static emlrtRSInfo c3_emlrtRSI = { 8,  /* lineNo */
  "Subsystem/Occup model",             /* fcnName */
  "#Building_Modeling_R2020a:321"      /* pathName */
};

static emlrtRSInfo c3_b_emlrtRSI = { 12,/* lineNo */
  "Subsystem/Occup model",             /* fcnName */
  "#Building_Modeling_R2020a:321"      /* pathName */
};

static emlrtRSInfo c3_c_emlrtRSI = { 21,/* lineNo */
  "Subsystem/Occup model",             /* fcnName */
  "#Building_Modeling_R2020a:321"      /* pathName */
};

static emlrtRSInfo c3_d_emlrtRSI = { 116,/* lineNo */
  "rng",                               /* fcnName */
  "C:\\Program Files\\MATLAB\\R2021a\\toolbox\\eml\\lib\\matlab\\randfun\\rng.m"/* pathName */
};

static emlrtRSInfo c3_e_emlrtRSI = { 114,/* lineNo */
  "rng",                               /* fcnName */
  "C:\\Program Files\\MATLAB\\R2021a\\toolbox\\eml\\lib\\matlab\\randfun\\rng.m"/* pathName */
};

static emlrtRSInfo c3_f_emlrtRSI = { 113,/* lineNo */
  "rng",                               /* fcnName */
  "C:\\Program Files\\MATLAB\\R2021a\\toolbox\\eml\\lib\\matlab\\randfun\\rng.m"/* pathName */
};

static emlrtRSInfo c3_g_emlrtRSI = { 54,/* lineNo */
  "rng",                               /* fcnName */
  "C:\\Program Files\\MATLAB\\R2021a\\toolbox\\eml\\lib\\matlab\\randfun\\rng.m"/* pathName */
};

static emlrtRSInfo c3_h_emlrtRSI = { 273,/* lineNo */
  "rng",                               /* fcnName */
  "C:\\Program Files\\MATLAB\\R2021a\\toolbox\\eml\\lib\\matlab\\randfun\\rng.m"/* pathName */
};

static emlrtRSInfo c3_i_emlrtRSI = { 275,/* lineNo */
  "rng",                               /* fcnName */
  "C:\\Program Files\\MATLAB\\R2021a\\toolbox\\eml\\lib\\matlab\\randfun\\rng.m"/* pathName */
};

static emlrtRSInfo c3_j_emlrtRSI = { 277,/* lineNo */
  "rng",                               /* fcnName */
  "C:\\Program Files\\MATLAB\\R2021a\\toolbox\\eml\\lib\\matlab\\randfun\\rng.m"/* pathName */
};

static emlrtRSInfo c3_k_emlrtRSI = { 278,/* lineNo */
  "rng",                               /* fcnName */
  "C:\\Program Files\\MATLAB\\R2021a\\toolbox\\eml\\lib\\matlab\\randfun\\rng.m"/* pathName */
};

static emlrtRSInfo c3_l_emlrtRSI = { 114,/* lineNo */
  "rand",                              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2021a\\toolbox\\eml\\lib\\matlab\\randfun\\rand.m"/* pathName */
};

static emlrtRSInfo c3_m_emlrtRSI = { 45,/* lineNo */
  "eml_rand",                          /* fcnName */
  "C:\\Program Files\\MATLAB\\R2021a\\toolbox\\eml\\lib\\matlab\\randfun\\private\\eml_rand.m"/* pathName */
};

static emlrtRSInfo c3_n_emlrtRSI = { 15,/* lineNo */
  "eml_rand_mt19937ar_stateful",       /* fcnName */
  "C:\\Program Files\\MATLAB\\R2021a\\toolbox\\eml\\lib\\matlab\\randfun\\private\\eml_rand_mt19937ar_stateful.m"/* pathName */
};

static emlrtRSInfo c3_o_emlrtRSI = { 51,/* lineNo */
  "eml_rand_mt19937ar",                /* fcnName */
  "C:\\Program Files\\MATLAB\\R2021a\\toolbox\\eml\\lib\\matlab\\randfun\\eml_rand_mt19937ar.m"/* pathName */
};

static emlrtRSInfo c3_p_emlrtRSI = { 15,/* lineNo */
  "sum",                               /* fcnName */
  "C:\\Program Files\\MATLAB\\R2021a\\toolbox\\eml\\lib\\matlab\\datafun\\sum.m"/* pathName */
};

static emlrtRSInfo c3_q_emlrtRSI = { 99,/* lineNo */
  "sumprod",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2021a\\toolbox\\eml\\lib\\matlab\\datafun\\private\\sumprod.m"/* pathName */
};

static emlrtRSInfo c3_r_emlrtRSI = { 138,/* lineNo */
  "combineVectorElements",             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2021a\\toolbox\\eml\\lib\\matlab\\datafun\\private\\combineVectorElements.m"/* pathName */
};

static emlrtRSInfo c3_s_emlrtRSI = { 198,/* lineNo */
  "combineVectorElements",             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2021a\\toolbox\\eml\\lib\\matlab\\datafun\\private\\combineVectorElements.m"/* pathName */
};

static emlrtRSInfo c3_t_emlrtRSI = { 21,/* lineNo */
  "eml_int_forloop_overflow_check",    /* fcnName */
  "C:\\Program Files\\MATLAB\\R2021a\\toolbox\\eml\\lib\\matlab\\eml\\eml_int_forloop_overflow_check.m"/* pathName */
};

static emlrtRTEInfo c3_emlrtRTEI = { 21,/* lineNo */
  30,                                  /* colNo */
  "Subsystem/Occup model",             /* fName */
  "#Building_Modeling_R2020a:321"      /* pName */
};

static emlrtRTEInfo c3_b_emlrtRTEI = { 12,/* lineNo */
  30,                                  /* colNo */
  "Subsystem/Occup model",             /* fName */
  "#Building_Modeling_R2020a:321"      /* pName */
};

static emlrtRTEInfo c3_c_emlrtRTEI = { 13,/* lineNo */
  24,                                  /* colNo */
  "eml_rand_mt19937ar_stateful",       /* fName */
  "C:\\Program Files\\MATLAB\\R2021a\\toolbox\\eml\\lib\\matlab\\randfun\\private\\eml_rand_mt19937ar_stateful.m"/* pName */
};

static emlrtBCInfo c3_emlrtBCI = { 1,  /* iFirst */
  10080,                               /* iLast */
  18,                                  /* lineNo */
  16,                                  /* colNo */
  "distribution_data",                 /* aName */
  "Subsystem/Occup model",             /* fName */
  "#Building_Modeling_R2020a:321",     /* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo c3_emlrtDCI = { 18, /* lineNo */
  16,                                  /* colNo */
  "Subsystem/Occup model",             /* fName */
  "#Building_Modeling_R2020a:321",     /* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo c3_b_emlrtBCI = { 1,/* iFirst */
  10080,                               /* iLast */
  20,                                  /* lineNo */
  21,                                  /* colNo */
  "distribution_data",                 /* aName */
  "Subsystem/Occup model",             /* fName */
  "#Building_Modeling_R2020a:321",     /* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo c3_b_emlrtDCI = { 20,/* lineNo */
  21,                                  /* colNo */
  "Subsystem/Occup model",             /* fName */
  "#Building_Modeling_R2020a:321",     /* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo c3_c_emlrtDCI = { 13,/* lineNo */
  30,                                  /* colNo */
  "eml_rand_mt19937ar_stateful",       /* fName */
  "C:\\Program Files\\MATLAB\\R2021a\\toolbox\\eml\\lib\\matlab\\randfun\\private\\eml_rand_mt19937ar_stateful.m",/* pName */
  4                                    /* checkKind */
};

static const uint32_T c3_uv[625] = { 5489U, 1301868182U, 2938499221U,
  2950281878U, 1875628136U, 751856242U, 944701696U, 2243192071U, 694061057U,
  219885934U, 2066767472U, 3182869408U, 485472502U, 2336857883U, 1071588843U,
  3418470598U, 951210697U, 3693558366U, 2923482051U, 1793174584U, 2982310801U,
  1586906132U, 1951078751U, 1808158765U, 1733897588U, 431328322U, 4202539044U,
  530658942U, 1714810322U, 3025256284U, 3342585396U, 1937033938U, 2640572511U,
  1654299090U, 3692403553U, 4233871309U, 3497650794U, 862629010U, 2943236032U,
  2426458545U, 1603307207U, 1133453895U, 3099196360U, 2208657629U, 2747653927U,
  931059398U, 761573964U, 3157853227U, 785880413U, 730313442U, 124945756U,
  2937117055U, 3295982469U, 1724353043U, 3021675344U, 3884886417U, 4010150098U,
  4056961966U, 699635835U, 2681338818U, 1339167484U, 720757518U, 2800161476U,
  2376097373U, 1532957371U, 3902664099U, 1238982754U, 3725394514U, 3449176889U,
  3570962471U, 4287636090U, 4087307012U, 3603343627U, 202242161U, 2995682783U,
  1620962684U, 3704723357U, 371613603U, 2814834333U, 2111005706U, 624778151U,
  2094172212U, 4284947003U, 1211977835U, 991917094U, 1570449747U, 2962370480U,
  1259410321U, 170182696U, 146300961U, 2836829791U, 619452428U, 2723670296U,
  1881399711U, 1161269684U, 1675188680U, 4132175277U, 780088327U, 3409462821U,
  1036518241U, 1834958505U, 3048448173U, 161811569U, 618488316U, 44795092U,
  3918322701U, 1924681712U, 3239478144U, 383254043U, 4042306580U, 2146983041U,
  3992780527U, 3518029708U, 3545545436U, 3901231469U, 1896136409U, 2028528556U,
  2339662006U, 501326714U, 2060962201U, 2502746480U, 561575027U, 581893337U,
  3393774360U, 1778912547U, 3626131687U, 2175155826U, 319853231U, 986875531U,
  819755096U, 2915734330U, 2688355739U, 3482074849U, 2736559U, 2296975761U,
  1029741190U, 2876812646U, 690154749U, 579200347U, 4027461746U, 1285330465U,
  2701024045U, 4117700889U, 759495121U, 3332270341U, 2313004527U, 2277067795U,
  4131855432U, 2722057515U, 1264804546U, 3848622725U, 2211267957U, 4100593547U,
  959123777U, 2130745407U, 3194437393U, 486673947U, 1377371204U, 17472727U,
  352317554U, 3955548058U, 159652094U, 1232063192U, 3835177280U, 49423123U,
  3083993636U, 733092U, 2120519771U, 2573409834U, 1112952433U, 3239502554U,
  761045320U, 1087580692U, 2540165110U, 641058802U, 1792435497U, 2261799288U,
  1579184083U, 627146892U, 2165744623U, 2200142389U, 2167590760U, 2381418376U,
  1793358889U, 3081659520U, 1663384067U, 2009658756U, 2689600308U, 739136266U,
  2304581039U, 3529067263U, 591360555U, 525209271U, 3131882996U, 294230224U,
  2076220115U, 3113580446U, 1245621585U, 1386885462U, 3203270426U, 123512128U,
  12350217U, 354956375U, 4282398238U, 3356876605U, 3888857667U, 157639694U,
  2616064085U, 1563068963U, 2762125883U, 4045394511U, 4180452559U, 3294769488U,
  1684529556U, 1002945951U, 3181438866U, 22506664U, 691783457U, 2685221343U,
  171579916U, 3878728600U, 2475806724U, 2030324028U, 3331164912U, 1708711359U,
  1970023127U, 2859691344U, 2588476477U, 2748146879U, 136111222U, 2967685492U,
  909517429U, 2835297809U, 3206906216U, 3186870716U, 341264097U, 2542035121U,
  3353277068U, 548223577U, 3170936588U, 1678403446U, 297435620U, 2337555430U,
  466603495U, 1132321815U, 1208589219U, 696392160U, 894244439U, 2562678859U,
  470224582U, 3306867480U, 201364898U, 2075966438U, 1767227936U, 2929737987U,
  3674877796U, 2654196643U, 3692734598U, 3528895099U, 2796780123U, 3048728353U,
  842329300U, 191554730U, 2922459673U, 3489020079U, 3979110629U, 1022523848U,
  2202932467U, 3583655201U, 3565113719U, 587085778U, 4176046313U, 3013713762U,
  950944241U, 396426791U, 3784844662U, 3477431613U, 3594592395U, 2782043838U,
  3392093507U, 3106564952U, 2829419931U, 1358665591U, 2206918825U, 3170783123U,
  31522386U, 2988194168U, 1782249537U, 1105080928U, 843500134U, 1225290080U,
  1521001832U, 3605886097U, 2802786495U, 2728923319U, 3996284304U, 903417639U,
  1171249804U, 1020374987U, 2824535874U, 423621996U, 1988534473U, 2493544470U,
  1008604435U, 1756003503U, 1488867287U, 1386808992U, 732088248U, 1780630732U,
  2482101014U, 976561178U, 1543448953U, 2602866064U, 2021139923U, 1952599828U,
  2360242564U, 2117959962U, 2753061860U, 2388623612U, 4138193781U, 2962920654U,
  2284970429U, 766920861U, 3457264692U, 2879611383U, 815055854U, 2332929068U,
  1254853997U, 3740375268U, 3799380844U, 4091048725U, 2006331129U, 1982546212U,
  686850534U, 1907447564U, 2682801776U, 2780821066U, 998290361U, 1342433871U,
  4195430425U, 607905174U, 3902331779U, 2454067926U, 1708133115U, 1170874362U,
  2008609376U, 3260320415U, 2211196135U, 433538229U, 2728786374U, 2189520818U,
  262554063U, 1182318347U, 3710237267U, 1221022450U, 715966018U, 2417068910U,
  2591870721U, 2870691989U, 3418190842U, 4238214053U, 1540704231U, 1575580968U,
  2095917976U, 4078310857U, 2313532447U, 2110690783U, 4056346629U, 4061784526U,
  1123218514U, 551538993U, 597148360U, 4120175196U, 3581618160U, 3181170517U,
  422862282U, 3227524138U, 1713114790U, 662317149U, 1230418732U, 928171837U,
  1324564878U, 1928816105U, 1786535431U, 2878099422U, 3290185549U, 539474248U,
  1657512683U, 552370646U, 1671741683U, 3655312128U, 1552739510U, 2605208763U,
  1441755014U, 181878989U, 3124053868U, 1447103986U, 3183906156U, 1728556020U,
  3502241336U, 3055466967U, 1013272474U, 818402132U, 1715099063U, 2900113506U,
  397254517U, 4194863039U, 1009068739U, 232864647U, 2540223708U, 2608288560U,
  2415367765U, 478404847U, 3455100648U, 3182600021U, 2115988978U, 434269567U,
  4117179324U, 3461774077U, 887256537U, 3545801025U, 286388911U, 3451742129U,
  1981164769U, 786667016U, 3310123729U, 3097811076U, 2224235657U, 2959658883U,
  3370969234U, 2514770915U, 3345656436U, 2677010851U, 2206236470U, 271648054U,
  2342188545U, 4292848611U, 3646533909U, 3754009956U, 3803931226U, 4160647125U,
  1477814055U, 4043852216U, 1876372354U, 3133294443U, 3871104810U, 3177020907U,
  2074304428U, 3479393793U, 759562891U, 164128153U, 1839069216U, 2114162633U,
  3989947309U, 3611054956U, 1333547922U, 835429831U, 494987340U, 171987910U,
  1252001001U, 370809172U, 3508925425U, 2535703112U, 1276855041U, 1922855120U,
  835673414U, 3030664304U, 613287117U, 171219893U, 3423096126U, 3376881639U,
  2287770315U, 1658692645U, 1262815245U, 3957234326U, 1168096164U, 2968737525U,
  2655813712U, 2132313144U, 3976047964U, 326516571U, 353088456U, 3679188938U,
  3205649712U, 2654036126U, 1249024881U, 880166166U, 691800469U, 2229503665U,
  1673458056U, 4032208375U, 1851778863U, 2563757330U, 376742205U, 1794655231U,
  340247333U, 1505873033U, 396524441U, 879666767U, 3335579166U, 3260764261U,
  3335999539U, 506221798U, 4214658741U, 975887814U, 2080536343U, 3360539560U,
  571586418U, 138896374U, 4234352651U, 2737620262U, 3928362291U, 1516365296U,
  38056726U, 3599462320U, 3585007266U, 3850961033U, 471667319U, 1536883193U,
  2310166751U, 1861637689U, 2530999841U, 4139843801U, 2710569485U, 827578615U,
  2012334720U, 2907369459U, 3029312804U, 2820112398U, 1965028045U, 35518606U,
  2478379033U, 643747771U, 1924139484U, 4123405127U, 3811735531U, 3429660832U,
  3285177704U, 1948416081U, 1311525291U, 1183517742U, 1739192232U, 3979815115U,
  2567840007U, 4116821529U, 213304419U, 4125718577U, 1473064925U, 2442436592U,
  1893310111U, 4195361916U, 3747569474U, 828465101U, 2991227658U, 750582866U,
  1205170309U, 1409813056U, 678418130U, 1171531016U, 3821236156U, 354504587U,
  4202874632U, 3882511497U, 1893248677U, 1903078632U, 26340130U, 2069166240U,
  3657122492U, 3725758099U, 831344905U, 811453383U, 3447711422U, 2434543565U,
  4166886888U, 3358210805U, 4142984013U, 2988152326U, 3527824853U, 982082992U,
  2809155763U, 190157081U, 3340214818U, 2365432395U, 2548636180U, 2894533366U,
  3474657421U, 2372634704U, 2845748389U, 43024175U, 2774226648U, 1987702864U,
  3186502468U, 453610222U, 4204736567U, 1392892630U, 2471323686U, 2470534280U,
  3541393095U, 4269885866U, 3909911300U, 759132955U, 1482612480U, 667715263U,
  1795580598U, 2337923983U, 3390586366U, 581426223U, 1515718634U, 476374295U,
  705213300U, 363062054U, 2084697697U, 2407503428U, 2292957699U, 2426213835U,
  2199989172U, 1987356470U, 4026755612U, 2147252133U, 270400031U, 1367820199U,
  2369854699U, 2844269403U, 79981964U, 624U };

/* Function Declarations */
static void initialize_c3_Building_Modeling_R2020a
  (SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance);
static void initialize_params_c3_Building_Modeling_R2020a
  (SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance);
static void enable_c3_Building_Modeling_R2020a
  (SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance);
static void disable_c3_Building_Modeling_R2020a
  (SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance);
static void c3_update_jit_animation_state_c3_Building_Modeling_R2020a
  (SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance);
static void c3_do_animation_call_c3_Building_Modeling_R2020a
  (SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance);
static void ext_mode_exec_c3_Building_Modeling_R2020a
  (SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance);
static const mxArray *get_sim_state_c3_Building_Modeling_R2020a
  (SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance);
static void set_sim_state_c3_Building_Modeling_R2020a
  (SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance, const mxArray
   *c3_st);
static void sf_gateway_c3_Building_Modeling_R2020a
  (SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance);
static void mdl_start_c3_Building_Modeling_R2020a
  (SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance);
static void mdl_terminate_c3_Building_Modeling_R2020a
  (SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance);
static void mdl_setup_runtime_resources_c3_Building_Modeling_R2020a
  (SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance);
static void mdl_cleanup_runtime_resources_c3_Building_Modeling_R2020a
  (SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance);
static void initSimStructsc3_Building_Modeling_R2020a
  (SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance);
static real_T c3_now(SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance);
static void c3_rand(SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance,
                    const emlrtStack *c3_sp, real_T c3_varargin_1,
                    c3_emxArray_real_T *c3_r);
static void c3_eml_rand_mt19937ar(SFc3_Building_Modeling_R2020aInstanceStruct
  *chartInstance, const emlrtStack *c3_sp, uint32_T c3_d_state[625], uint32_T
  c3_e_state[625], real_T *c3_r);
static int32_T c3_combineVectorElements
  (SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance, const emlrtStack *
   c3_sp, c3_emxArray_boolean_T *c3_x);
static void c3_check_forloop_overflow_error
  (SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance, const emlrtStack *
   c3_sp);
static real_T c3_emlrt_marshallIn(SFc3_Building_Modeling_R2020aInstanceStruct
  *chartInstance, const mxArray *c3_b_Occup, const char_T *c3_identifier);
static real_T c3_b_emlrt_marshallIn(SFc3_Building_Modeling_R2020aInstanceStruct *
  chartInstance, const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId);
static real_T c3_c_emlrt_marshallIn(SFc3_Building_Modeling_R2020aInstanceStruct *
  chartInstance, const mxArray *c3_b_last_ind, const char_T *c3_identifier,
  boolean_T *c3_svPtr);
static real_T c3_d_emlrt_marshallIn(SFc3_Building_Modeling_R2020aInstanceStruct *
  chartInstance, const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId,
  boolean_T *c3_svPtr);
static uint32_T c3_e_emlrt_marshallIn
  (SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance, const mxArray
   *c3_b_method, const char_T *c3_identifier, boolean_T *c3_svPtr);
static uint32_T c3_f_emlrt_marshallIn
  (SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance, const mxArray
   *c3_u, const emlrtMsgIdentifier *c3_parentId, boolean_T *c3_svPtr);
static void c3_g_emlrt_marshallIn(SFc3_Building_Modeling_R2020aInstanceStruct
  *chartInstance, const mxArray *c3_d_state, const char_T *c3_identifier,
  boolean_T *c3_svPtr, uint32_T c3_y[625]);
static void c3_h_emlrt_marshallIn(SFc3_Building_Modeling_R2020aInstanceStruct
  *chartInstance, const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId,
  boolean_T *c3_svPtr, uint32_T c3_y[625]);
static void c3_i_emlrt_marshallIn(SFc3_Building_Modeling_R2020aInstanceStruct
  *chartInstance, const mxArray *c3_d_state, const char_T *c3_identifier,
  boolean_T *c3_svPtr, uint32_T c3_y[2]);
static void c3_j_emlrt_marshallIn(SFc3_Building_Modeling_R2020aInstanceStruct
  *chartInstance, const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId,
  boolean_T *c3_svPtr, uint32_T c3_y[2]);
static uint8_T c3_k_emlrt_marshallIn(SFc3_Building_Modeling_R2020aInstanceStruct
  *chartInstance, const mxArray *c3_b_is_active_c3_Building_Modeling_R2020a,
  const char_T *c3_identifier);
static uint8_T c3_l_emlrt_marshallIn(SFc3_Building_Modeling_R2020aInstanceStruct
  *chartInstance, const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId);
static void c3_chart_data_browse_helper
  (SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance, int32_T
   c3_ssIdNumber, const mxArray **c3_mxData, uint8_T *c3_isValueTooBig);
static real_T c3_b_eml_rand_mt19937ar
  (SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance, const emlrtStack *
   c3_sp, uint32_T c3_d_state[625]);
static void c3_emxEnsureCapacity_boolean_T
  (SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance, const emlrtStack *
   c3_sp, c3_emxArray_boolean_T *c3_emxArray, int32_T c3_oldNumel, const
   emlrtRTEInfo *c3_srcLocation);
static void c3_emxInit_boolean_T(SFc3_Building_Modeling_R2020aInstanceStruct
  *chartInstance, const emlrtStack *c3_sp, c3_emxArray_boolean_T **c3_pEmxArray,
  int32_T c3_numDimensions, const emlrtRTEInfo *c3_srcLocation);
static void c3_emxInit_real_T(SFc3_Building_Modeling_R2020aInstanceStruct
  *chartInstance, const emlrtStack *c3_sp, c3_emxArray_real_T **c3_pEmxArray,
  int32_T c3_numDimensions, const emlrtRTEInfo *c3_srcLocation);
static void c3_emxFree_boolean_T(SFc3_Building_Modeling_R2020aInstanceStruct
  *chartInstance, c3_emxArray_boolean_T **c3_pEmxArray);
static void c3_emxFree_real_T(SFc3_Building_Modeling_R2020aInstanceStruct
  *chartInstance, c3_emxArray_real_T **c3_pEmxArray);
static void c3_emxEnsureCapacity_real_T
  (SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance, const emlrtStack *
   c3_sp, c3_emxArray_real_T *c3_emxArray, int32_T c3_oldNumel, const
   emlrtRTEInfo *c3_srcLocation);
static void init_dsm_address_info(SFc3_Building_Modeling_R2020aInstanceStruct
  *chartInstance);
static void init_simulink_io_address(SFc3_Building_Modeling_R2020aInstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c3_Building_Modeling_R2020a
  (SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance)
{
  sim_mode_is_external(chartInstance->S);
  chartInstance->c3_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c3_rng_init_not_empty = false;
  chartInstance->c3_last_ind_not_empty = false;
  chartInstance->c3_persons_inside_not_empty = false;
  chartInstance->c3_seed_not_empty = false;
  chartInstance->c3_method_not_empty = false;
  chartInstance->c3_state_not_empty = false;
  chartInstance->c3_b_state_not_empty = false;
  chartInstance->c3_c_state_not_empty = false;
  chartInstance->c3_is_active_c3_Building_Modeling_R2020a = 0U;
}

static void initialize_params_c3_Building_Modeling_R2020a
  (SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance)
{
  real_T c3_dv[20160];
  real_T c3_d;
  int32_T c3_i;
  sf_mex_import_named("distribution_data", sf_mex_get_sfun_param
                      (chartInstance->S, 0U, 0U), c3_dv, 0, 0, 0U, 1, 0U, 2,
                      10080, 2);
  for (c3_i = 0; c3_i < 20160; c3_i++) {
    chartInstance->c3_distribution_data[c3_i] = c3_dv[c3_i];
  }

  sf_mex_import_named("persons_max", sf_mex_get_sfun_param(chartInstance->S, 1U,
    0U), &c3_d, 0, 0, 0U, 0, 0U, 0);
  chartInstance->c3_persons_max = c3_d;
}

static void enable_c3_Building_Modeling_R2020a
  (SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c3_Building_Modeling_R2020a
  (SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c3_update_jit_animation_state_c3_Building_Modeling_R2020a
  (SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c3_do_animation_call_c3_Building_Modeling_R2020a
  (SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance)
{
  sfDoAnimationWrapper(chartInstance->S, false, true);
  sfDoAnimationWrapper(chartInstance->S, false, false);
}

static void ext_mode_exec_c3_Building_Modeling_R2020a
  (SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *get_sim_state_c3_Building_Modeling_R2020a
  (SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance)
{
  const mxArray *c3_b_y = NULL;
  const mxArray *c3_c_y = NULL;
  const mxArray *c3_d_y = NULL;
  const mxArray *c3_e_y = NULL;
  const mxArray *c3_f_y = NULL;
  const mxArray *c3_g_y = NULL;
  const mxArray *c3_h_y = NULL;
  const mxArray *c3_i_y = NULL;
  const mxArray *c3_j_y = NULL;
  const mxArray *c3_k_y = NULL;
  const mxArray *c3_st;
  const mxArray *c3_y = NULL;
  c3_st = NULL;
  c3_st = NULL;
  c3_y = NULL;
  sf_mex_assign(&c3_y, sf_mex_createcellmatrix(10, 1), false);
  c3_b_y = NULL;
  sf_mex_assign(&c3_b_y, sf_mex_create("y", chartInstance->c3_Occup, 0, 0U, 0U,
    0U, 0), false);
  sf_mex_setcell(c3_y, 0, c3_b_y);
  c3_c_y = NULL;
  if (!chartInstance->c3_last_ind_not_empty) {
    sf_mex_assign(&c3_c_y, sf_mex_create("y", NULL, 0, 0U, 1U, 0U, 2, 0, 0),
                  false);
  } else {
    sf_mex_assign(&c3_c_y, sf_mex_create("y", &chartInstance->c3_last_ind, 0, 0U,
      0U, 0U, 0), false);
  }

  sf_mex_setcell(c3_y, 1, c3_c_y);
  c3_d_y = NULL;
  if (!chartInstance->c3_method_not_empty) {
    sf_mex_assign(&c3_d_y, sf_mex_create("y", NULL, 0, 0U, 1U, 0U, 2, 0, 0),
                  false);
  } else {
    sf_mex_assign(&c3_d_y, sf_mex_create("y", &chartInstance->c3_method, 7, 0U,
      0U, 0U, 0), false);
  }

  sf_mex_setcell(c3_y, 2, c3_d_y);
  c3_e_y = NULL;
  if (!chartInstance->c3_last_ind_not_empty) {
    sf_mex_assign(&c3_e_y, sf_mex_create("y", NULL, 0, 0U, 1U, 0U, 2, 0, 0),
                  false);
  } else {
    sf_mex_assign(&c3_e_y, sf_mex_create("y", &chartInstance->c3_persons_inside,
      0, 0U, 0U, 0U, 0), false);
  }

  sf_mex_setcell(c3_y, 3, c3_e_y);
  c3_f_y = NULL;
  if (!chartInstance->c3_last_ind_not_empty) {
    sf_mex_assign(&c3_f_y, sf_mex_create("y", NULL, 0, 0U, 1U, 0U, 2, 0, 0),
                  false);
  } else {
    sf_mex_assign(&c3_f_y, sf_mex_create("y", &chartInstance->c3_rng_init, 0, 0U,
      0U, 0U, 0), false);
  }

  sf_mex_setcell(c3_y, 4, c3_f_y);
  c3_g_y = NULL;
  if (!chartInstance->c3_method_not_empty) {
    sf_mex_assign(&c3_g_y, sf_mex_create("y", NULL, 0, 0U, 1U, 0U, 2, 0, 0),
                  false);
  } else {
    sf_mex_assign(&c3_g_y, sf_mex_create("y", &chartInstance->c3_seed, 7, 0U, 0U,
      0U, 0), false);
  }

  sf_mex_setcell(c3_y, 5, c3_g_y);
  c3_h_y = NULL;
  if (!chartInstance->c3_method_not_empty) {
    sf_mex_assign(&c3_h_y, sf_mex_create("y", NULL, 0, 0U, 1U, 0U, 2, 0, 0),
                  false);
  } else {
    sf_mex_assign(&c3_h_y, sf_mex_create("y", &chartInstance->c3_c_state, 7, 0U,
      0U, 0U, 0), false);
  }

  sf_mex_setcell(c3_y, 6, c3_h_y);
  c3_i_y = NULL;
  if (!chartInstance->c3_state_not_empty) {
    sf_mex_assign(&c3_i_y, sf_mex_create("y", NULL, 0, 0U, 1U, 0U, 2, 0, 0),
                  false);
  } else {
    sf_mex_assign(&c3_i_y, sf_mex_create("y", chartInstance->c3_state, 7, 0U, 1U,
      0U, 1, 625), false);
  }

  sf_mex_setcell(c3_y, 7, c3_i_y);
  c3_j_y = NULL;
  if (!chartInstance->c3_b_state_not_empty) {
    sf_mex_assign(&c3_j_y, sf_mex_create("y", NULL, 0, 0U, 1U, 0U, 2, 0, 0),
                  false);
  } else {
    sf_mex_assign(&c3_j_y, sf_mex_create("y", chartInstance->c3_b_state, 7, 0U,
      1U, 0U, 1, 2), false);
  }

  sf_mex_setcell(c3_y, 8, c3_j_y);
  c3_k_y = NULL;
  sf_mex_assign(&c3_k_y, sf_mex_create("y",
    &chartInstance->c3_is_active_c3_Building_Modeling_R2020a, 3, 0U, 0U, 0U, 0),
                false);
  sf_mex_setcell(c3_y, 9, c3_k_y);
  sf_mex_assign(&c3_st, c3_y, false);
  return c3_st;
}

static void set_sim_state_c3_Building_Modeling_R2020a
  (SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance, const mxArray
   *c3_st)
{
  const mxArray *c3_u;
  int32_T c3_i;
  int32_T c3_i1;
  uint32_T c3_b_uv[625];
  uint32_T c3_uv1[2];
  chartInstance->c3_doneDoubleBufferReInit = true;
  c3_u = sf_mex_dup(c3_st);
  *chartInstance->c3_Occup = c3_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell(c3_u, 0)), "Occup");
  chartInstance->c3_last_ind = c3_c_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell(c3_u, 1)), "last_ind", &chartInstance->c3_last_ind_not_empty);
  chartInstance->c3_method = c3_e_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell(c3_u, 2)), "method", &chartInstance->c3_method_not_empty);
  chartInstance->c3_persons_inside = c3_c_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c3_u, 3)), "persons_inside",
    &chartInstance->c3_persons_inside_not_empty);
  chartInstance->c3_rng_init = c3_c_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell(c3_u, 4)), "rng_init", &chartInstance->c3_rng_init_not_empty);
  chartInstance->c3_seed = c3_e_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell(c3_u, 5)), "seed", &chartInstance->c3_seed_not_empty);
  chartInstance->c3_c_state = c3_e_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell(c3_u, 6)), "state", &chartInstance->c3_c_state_not_empty);
  c3_g_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c3_u, 7)),
                        "state", &chartInstance->c3_state_not_empty, c3_b_uv);
  for (c3_i = 0; c3_i < 625; c3_i++) {
    chartInstance->c3_state[c3_i] = c3_b_uv[c3_i];
  }

  c3_i_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c3_u, 8)),
                        "state", &chartInstance->c3_b_state_not_empty, c3_uv1);
  for (c3_i1 = 0; c3_i1 < 2; c3_i1++) {
    chartInstance->c3_b_state[c3_i1] = c3_uv1[c3_i1];
  }

  chartInstance->c3_is_active_c3_Building_Modeling_R2020a =
    c3_k_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c3_u, 9)),
    "is_active_c3_Building_Modeling_R2020a");
  sf_mex_destroy(&c3_u);
  sf_mex_destroy(&c3_st);
}

static void sf_gateway_c3_Building_Modeling_R2020a
  (SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance)
{
  static char_T c3_cv[36] = { 'C', 'o', 'd', 'e', 'r', ':', 't', 'o', 'o', 'l',
    'b', 'o', 'x', ':', 'a', 'u', 't', 'o', 'D', 'i', 'm', 'I', 'n', 'c', 'o',
    'm', 'p', 'a', 't', 'i', 'b', 'i', 'l', 'i', 't', 'y' };

  static char_T c3_cv1[36] = { 'C', 'o', 'd', 'e', 'r', ':', 't', 'o', 'o', 'l',
    'b', 'o', 'x', ':', 'a', 'u', 't', 'o', 'D', 'i', 'm', 'I', 'n', 'c', 'o',
    'm', 'p', 'a', 't', 'i', 'b', 'i', 'l', 'i', 't', 'y' };

  time_t c3_b_eTime;
  time_t c3_eTime;
  c3_emxArray_boolean_T *c3_x;
  c3_emxArray_real_T *c3_r;
  emlrtStack c3_b_st;
  emlrtStack c3_c_st;
  emlrtStack c3_d_st;
  emlrtStack c3_st = { NULL,           /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  const mxArray *c3_b_y = NULL;
  const mxArray *c3_d_y = NULL;
  const mxArray *c3_e_y = NULL;
  const mxArray *c3_y = NULL;
  real_T c3_b_distribution_data[20160];
  real_T c3_a;
  real_T c3_b_Occup;
  real_T c3_b_a;
  real_T c3_b_mti;
  real_T c3_b_persons_max;
  real_T c3_b_time;
  real_T c3_b_x;
  real_T c3_c_x;
  real_T c3_c_y;
  real_T c3_current_P;
  real_T c3_d;
  real_T c3_d1;
  real_T c3_d2;
  real_T c3_d_x;
  real_T c3_e_x;
  real_T c3_f_x;
  real_T c3_f_y;
  real_T c3_g_x;
  real_T c3_h_x;
  real_T c3_i_x;
  real_T c3_j_x;
  real_T c3_k_x;
  real_T c3_s;
  real_T c3_s0;
  int32_T c3_b_loop_ub;
  int32_T c3_b_nz;
  int32_T c3_exitg1;
  int32_T c3_i;
  int32_T c3_i1;
  int32_T c3_i2;
  int32_T c3_i3;
  int32_T c3_i4;
  int32_T c3_i5;
  int32_T c3_i6;
  int32_T c3_i7;
  int32_T c3_loop_ub;
  int32_T c3_mti;
  int32_T c3_nz;
  int32_T c3_prevEpochTime;
  int32_T c3_t;
  uint32_T c3_arg3;
  uint32_T c3_b_r;
  uint32_T c3_b_seed;
  uint32_T c3_u;
  uint32_T c3_u1;
  uint32_T c3_varargin_1;
  boolean_T c3_b;
  boolean_T c3_b1;
  boolean_T c3_b_rEQ0;
  boolean_T c3_covSaturation;
  boolean_T c3_rEQ0;
  c3_st.tls = chartInstance->c3_fEmlrtCtx;
  c3_b_st.prev = &c3_st;
  c3_b_st.tls = c3_st.tls;
  c3_c_st.prev = &c3_b_st;
  c3_c_st.tls = c3_b_st.tls;
  c3_d_st.prev = &c3_c_st;
  c3_d_st.tls = c3_c_st.tls;
  chartInstance->c3_JITTransitionAnimation[0] = 0U;
  _sfTime_ = sf_get_time(chartInstance->S);
  covrtSigUpdateFcn(chartInstance->c3_covrtInstance, 0U, *chartInstance->c3_time);
  chartInstance->c3_sfEvent = CALL_EVENT;
  c3_b_time = *chartInstance->c3_time;
  for (c3_i = 0; c3_i < 20160; c3_i++) {
    c3_b_distribution_data[c3_i] = chartInstance->c3_distribution_data[c3_i];
  }

  c3_b_persons_max = chartInstance->c3_persons_max;
  covrtEmlFcnEval(chartInstance->c3_covrtInstance, 4U, 0, 0);
  c3_emxInit_boolean_T(chartInstance, &c3_st, &c3_x, 1, &c3_b_emlrtRTEI);
  c3_emxInit_real_T(chartInstance, &c3_st, &c3_r, 1, &c3_b_emlrtRTEI);
  if (covrtEmlIfEval(chartInstance->c3_covrtInstance, 4U, 0, 0,
                     !chartInstance->c3_rng_init_not_empty)) {
    c3_b_st.site = &c3_emlrtRSI;
    if (!chartInstance->c3_seed_not_empty) {
      chartInstance->c3_seed = 0U;
      chartInstance->c3_seed_not_empty = true;
    }

    c3_c_st.site = &c3_g_emlrtRSI;
    if (!chartInstance->c3_method_not_empty) {
      chartInstance->c3_method = 7U;
      chartInstance->c3_method_not_empty = true;
    }

    c3_c_st.site = &c3_f_emlrtRSI;
    c3_d_st.site = &c3_h_emlrtRSI;
    c3_b_x = c3_now(chartInstance) * 8.64E+6;
    c3_c_x = c3_b_x;
    c3_c_x = muDoubleScalarFloor(c3_c_x);
    c3_d_x = c3_c_x;
    c3_a = c3_d_x;
    c3_e_x = c3_a;
    c3_f_x = c3_e_x;
    if (c3_f_x == 0.0) {
      c3_s = 0.0;
    } else {
      c3_s = muDoubleScalarRem(c3_f_x, 2.147483647E+9);
      c3_rEQ0 = (c3_s == 0.0);
      if (c3_rEQ0) {
        c3_s = 0.0;
      } else if (c3_f_x < 0.0) {
        c3_s += 2.147483647E+9;
      }
    }

    c3_d_st.site = &c3_i_emlrtRSI;
    c3_eTime = time(NULL);
    c3_prevEpochTime = (int32_T)c3_eTime + 1;
    do {
      c3_exitg1 = 0;
      c3_d_st.site = &c3_j_emlrtRSI;
      c3_b_eTime = time(NULL);
      c3_t = (int32_T)c3_b_eTime;
      if (c3_t <= c3_prevEpochTime) {
        c3_d_st.site = &c3_k_emlrtRSI;
        c3_g_x = c3_now(chartInstance) * 8.64E+6;
        c3_h_x = c3_g_x;
        c3_h_x = muDoubleScalarFloor(c3_h_x);
        c3_i_x = c3_h_x;
        c3_b_a = c3_i_x;
        c3_j_x = c3_b_a;
        c3_k_x = c3_j_x;
        if (c3_k_x == 0.0) {
          c3_s0 = 0.0;
        } else {
          c3_s0 = muDoubleScalarRem(c3_k_x, 2.147483647E+9);
          c3_b_rEQ0 = (c3_s0 == 0.0);
          if (c3_b_rEQ0) {
            c3_s0 = 0.0;
          } else if (c3_k_x < 0.0) {
            c3_s0 += 2.147483647E+9;
          }
        }

        if (c3_s != c3_s0) {
          c3_exitg1 = 1;
        }
      } else {
        c3_exitg1 = 1;
      }
    } while (c3_exitg1 == 0);

    c3_d1 = muDoubleScalarRound(c3_s);
    c3_covSaturation = false;
    if (c3_d1 < 4.294967296E+9) {
      if (c3_d1 >= 0.0) {
        c3_u = (uint32_T)c3_d1;
      } else {
        c3_covSaturation = true;
        c3_u = 0U;
        sf_data_saturate_error(chartInstance->S, 1U, 194, 14);
      }
    } else if (c3_d1 >= 4.294967296E+9) {
      c3_covSaturation = true;
      c3_u = MAX_uint32_T;
      sf_data_saturate_error(chartInstance->S, 1U, 194, 14);
    } else {
      c3_u = 0U;
    }

    covrtSaturationUpdateFcn(chartInstance->c3_covrtInstance, 4, 0, 0, 0,
      c3_covSaturation);
    chartInstance->c3_seed = c3_u;
    c3_c_st.site = &c3_e_emlrtRSI;
    if (!chartInstance->c3_method_not_empty) {
      chartInstance->c3_method = 7U;
      chartInstance->c3_method_not_empty = true;
    }

    c3_c_st.site = &c3_d_emlrtRSI;
    c3_varargin_1 = chartInstance->c3_seed;
    if (!chartInstance->c3_state_not_empty) {
      for (c3_i5 = 0; c3_i5 < 625; c3_i5++) {
        chartInstance->c3_state[c3_i5] = c3_uv[c3_i5];
      }

      chartInstance->c3_state_not_empty = true;
    }

    c3_arg3 = c3_varargin_1;
    c3_b_seed = c3_arg3;
    c3_b_r = c3_b_seed;
    chartInstance->c3_state[0] = c3_b_seed;
    for (c3_mti = 0; c3_mti < 623; c3_mti++) {
      c3_b_mti = (real_T)c3_mti + 2.0;
      c3_d2 = muDoubleScalarRound(c3_b_mti - 1.0);
      if (c3_d2 < 4.294967296E+9) {
        if (c3_d2 >= 0.0) {
          c3_u1 = (uint32_T)c3_d2;
        } else {
          c3_u1 = 0U;
          sf_data_saturate_error(chartInstance->S, 1U, 194, 14);
        }
      } else if (c3_d2 >= 4.294967296E+9) {
        c3_u1 = MAX_uint32_T;
        sf_data_saturate_error(chartInstance->S, 1U, 194, 14);
      } else {
        c3_u1 = 0U;
      }

      c3_b_r = (c3_b_r ^ c3_b_r >> 30U) * 1812433253U + c3_u1;
      chartInstance->c3_state[(int32_T)c3_b_mti - 1] = c3_b_r;
    }

    chartInstance->c3_state[624] = 624U;
    chartInstance->c3_rng_init = 1.0;
    chartInstance->c3_rng_init_not_empty = true;
    c3_current_P = c3_b_distribution_data[10080];
    c3_b_st.site = &c3_b_emlrtRSI;
    c3_c_st.site = &c3_b_emlrtRSI;
    c3_rand(chartInstance, &c3_c_st, c3_b_persons_max, c3_r);
    c3_i6 = c3_x->size[0];
    c3_x->size[0] = c3_r->size[0];
    c3_emxEnsureCapacity_boolean_T(chartInstance, &c3_b_st, c3_x, c3_i6,
      &c3_b_emlrtRTEI);
    c3_b_loop_ub = c3_r->size[0] - 1;
    for (c3_i7 = 0; c3_i7 <= c3_b_loop_ub; c3_i7++) {
      c3_x->data[c3_i7] = (c3_r->data[c3_i7] < c3_current_P);
    }

    c3_c_st.site = &c3_p_emlrtRSI;
    if ((c3_x->size[0] == 1) || ((real_T)c3_x->size[0] != 1.0)) {
      c3_b1 = true;
    } else {
      c3_b1 = false;
    }

    if (!c3_b1) {
      c3_d_y = NULL;
      sf_mex_assign(&c3_d_y, sf_mex_create("y", c3_cv1, 10, 0U, 1U, 0U, 2, 1, 36),
                    false);
      c3_e_y = NULL;
      sf_mex_assign(&c3_e_y, sf_mex_create("y", c3_cv1, 10, 0U, 1U, 0U, 2, 1, 36),
                    false);
      sf_mex_call(&c3_c_st, &c3_b_emlrtMCI, "error", 0U, 2U, 14, c3_d_y, 14,
                  sf_mex_call(&c3_c_st, NULL, "getString", 1U, 1U, 14,
        sf_mex_call(&c3_c_st, NULL, "message", 1U, 1U, 14, c3_e_y)));
    }

    c3_d_st.site = &c3_q_emlrtRSI;
    c3_b_nz = c3_combineVectorElements(chartInstance, &c3_d_st, c3_x);
    c3_f_y = (real_T)c3_b_nz;
    chartInstance->c3_persons_inside = c3_f_y;
    chartInstance->c3_persons_inside_not_empty = true;
    chartInstance->c3_last_ind = 1.0;
    chartInstance->c3_last_ind_not_empty = true;
  }

  if (chartInstance->c3_last_ind != (real_T)(int32_T)muDoubleScalarFloor
      (chartInstance->c3_last_ind)) {
    emlrtIntegerCheckR2012b(chartInstance->c3_last_ind, &c3_emlrtDCI, &c3_st);
  }

  c3_i1 = (int32_T)chartInstance->c3_last_ind;
  if ((c3_i1 < 1) || (c3_i1 > 10080)) {
    emlrtDynamicBoundsCheckR2012b(c3_i1, 1, 10080, &c3_emlrtBCI, &c3_st);
  }

  c3_d = c3_b_distribution_data[c3_i1 - 1];
  if (covrtEmlIfEval(chartInstance->c3_covrtInstance, 4U, 0, 1,
                     covrtRelationalopUpdateFcn(chartInstance->c3_covrtInstance,
        4U, 0U, 0U, c3_b_time, c3_d, -1, 5U, c3_b_time >= c3_d))) {
    chartInstance->c3_last_ind++;
    if (chartInstance->c3_last_ind != (real_T)(int32_T)muDoubleScalarFloor
        (chartInstance->c3_last_ind)) {
      emlrtIntegerCheckR2012b(chartInstance->c3_last_ind, &c3_b_emlrtDCI, &c3_st);
    }

    c3_i2 = (int32_T)chartInstance->c3_last_ind;
    if ((c3_i2 < 1) || (c3_i2 > 10080)) {
      emlrtDynamicBoundsCheckR2012b(c3_i2, 1, 10080, &c3_b_emlrtBCI, &c3_st);
    }

    c3_current_P = c3_b_distribution_data[c3_i2 + 10079];
    c3_b_st.site = &c3_c_emlrtRSI;
    c3_c_st.site = &c3_c_emlrtRSI;
    c3_rand(chartInstance, &c3_c_st, c3_b_persons_max, c3_r);
    c3_i3 = c3_x->size[0];
    c3_x->size[0] = c3_r->size[0];
    c3_emxEnsureCapacity_boolean_T(chartInstance, &c3_b_st, c3_x, c3_i3,
      &c3_emlrtRTEI);
    c3_loop_ub = c3_r->size[0] - 1;
    for (c3_i4 = 0; c3_i4 <= c3_loop_ub; c3_i4++) {
      c3_x->data[c3_i4] = (c3_r->data[c3_i4] < c3_current_P);
    }

    c3_c_st.site = &c3_p_emlrtRSI;
    if ((c3_x->size[0] == 1) || ((real_T)c3_x->size[0] != 1.0)) {
      c3_b = true;
    } else {
      c3_b = false;
    }

    if (!c3_b) {
      c3_y = NULL;
      sf_mex_assign(&c3_y, sf_mex_create("y", c3_cv, 10, 0U, 1U, 0U, 2, 1, 36),
                    false);
      c3_b_y = NULL;
      sf_mex_assign(&c3_b_y, sf_mex_create("y", c3_cv, 10, 0U, 1U, 0U, 2, 1, 36),
                    false);
      sf_mex_call(&c3_c_st, &c3_b_emlrtMCI, "error", 0U, 2U, 14, c3_y, 14,
                  sf_mex_call(&c3_c_st, NULL, "getString", 1U, 1U, 14,
        sf_mex_call(&c3_c_st, NULL, "message", 1U, 1U, 14, c3_b_y)));
    }

    c3_d_st.site = &c3_q_emlrtRSI;
    c3_nz = c3_combineVectorElements(chartInstance, &c3_d_st, c3_x);
    c3_c_y = (real_T)c3_nz;
    chartInstance->c3_persons_inside = c3_c_y;
  }

  c3_emxFree_real_T(chartInstance, &c3_r);
  c3_emxFree_boolean_T(chartInstance, &c3_x);
  c3_b_Occup = chartInstance->c3_persons_inside;
  *chartInstance->c3_Occup = c3_b_Occup;
  c3_do_animation_call_c3_Building_Modeling_R2020a(chartInstance);
  covrtSigUpdateFcn(chartInstance->c3_covrtInstance, 1U,
                    *chartInstance->c3_Occup);
}

static void mdl_start_c3_Building_Modeling_R2020a
  (SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void mdl_terminate_c3_Building_Modeling_R2020a
  (SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void mdl_setup_runtime_resources_c3_Building_Modeling_R2020a
  (SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance)
{
  static const uint32_T c3_decisionTxtEndIdx = 0U;
  static const uint32_T c3_decisionTxtStartIdx = 0U;
  setLegacyDebuggerFlag(chartInstance->S, false);
  setDebuggerFlag(chartInstance->S, true);
  setDataBrowseFcn(chartInstance->S, (void *)&c3_chart_data_browse_helper);
  chartInstance->c3_RuntimeVar = sfListenerCacheSimStruct(chartInstance->S);
  sfListenerInitializeRuntimeVars(chartInstance->c3_RuntimeVar,
    &chartInstance->c3_IsDebuggerActive,
    &chartInstance->c3_IsSequenceViewerPresent, 0, 0,
    &chartInstance->c3_mlFcnLineNumber, &chartInstance->c3_IsHeatMapPresent, 0);
  sim_mode_is_external(chartInstance->S);
  covrtCreateStateflowInstanceData(chartInstance->c3_covrtInstance, 1U, 0U, 1U,
    8U);
  covrtChartInitFcn(chartInstance->c3_covrtInstance, 0U, false, false, false);
  covrtStateInitFcn(chartInstance->c3_covrtInstance, 0U, 0U, false, false, false,
                    0U, &c3_decisionTxtStartIdx, &c3_decisionTxtEndIdx);
  covrtTransInitFcn(chartInstance->c3_covrtInstance, 0U, 0, NULL, NULL, 0U, NULL);
  covrtEmlInitFcn(chartInstance->c3_covrtInstance, "", 4U, 0U, 1U, 0U, 2U, 0U,
                  1U, 0U, 0U, 0U, 0U, 0U);
  covrtEmlFcnInitFcn(chartInstance->c3_covrtInstance, 4U, 0U, 0U,
                     "eML_blk_kernel", 0, -1, 623);
  covrtEmlSaturationInitFcn(chartInstance->c3_covrtInstance, 4U, 0U, 0U, 194, -1,
    208);
  covrtEmlIfInitFcn(chartInstance->c3_covrtInstance, 4U, 0U, 0U, 165, 185, -1,
                    382);
  covrtEmlIfInitFcn(chartInstance->c3_covrtInstance, 4U, 0U, 1U, 397, 437, -1,
                    590);
  covrtEmlRelationalInitFcn(chartInstance->c3_covrtInstance, 4U, 0U, 0U, 400,
    437, -1, 5U);
}

static void mdl_cleanup_runtime_resources_c3_Building_Modeling_R2020a
  (SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance)
{
  sfListenerLightTerminate(chartInstance->c3_RuntimeVar);
  covrtDeleteStateflowInstanceData(chartInstance->c3_covrtInstance);
}

static void initSimStructsc3_Building_Modeling_R2020a
  (SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

const mxArray *sf_c3_Building_Modeling_R2020a_get_eml_resolved_functions_info
  (void)
{
  const mxArray *c3_nameCaptureInfo = NULL;
  c3_nameCaptureInfo = NULL;
  sf_mex_assign(&c3_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c3_nameCaptureInfo;
}

static real_T c3_now(SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance)
{
  time_t c3_rawtime;
  c3_sxaDueAh1f53T9ESYg9Uc4E c3_timeinfoDouble;
  struct tm c3_timeinfo;
  real_T c3_cDaysMonthWise[12];
  real_T c3_a;
  real_T c3_b_a;
  real_T c3_b_r;
  real_T c3_b_x;
  real_T c3_c_a;
  real_T c3_c_r;
  real_T c3_c_x;
  real_T c3_dDateNum;
  real_T c3_d_x;
  real_T c3_e_x;
  real_T c3_f_x;
  real_T c3_g_x;
  real_T c3_h_x;
  real_T c3_i_x;
  real_T c3_j_x;
  real_T c3_k_x;
  real_T c3_l_x;
  real_T c3_m_x;
  real_T c3_n_x;
  real_T c3_o_x;
  real_T c3_r;
  real_T c3_x;
  boolean_T c3_b_rEQ0;
  boolean_T c3_c_rEQ0;
  boolean_T c3_guard1 = false;
  boolean_T c3_rEQ0;
  (void)chartInstance;
  c3_cDaysMonthWise[0] = 0.0;
  c3_cDaysMonthWise[1] = 31.0;
  c3_cDaysMonthWise[2] = 59.0;
  c3_cDaysMonthWise[3] = 90.0;
  c3_cDaysMonthWise[4] = 120.0;
  c3_cDaysMonthWise[5] = 151.0;
  c3_cDaysMonthWise[6] = 181.0;
  c3_cDaysMonthWise[7] = 212.0;
  c3_cDaysMonthWise[8] = 243.0;
  c3_cDaysMonthWise[9] = 273.0;
  c3_cDaysMonthWise[10] = 304.0;
  c3_cDaysMonthWise[11] = 334.0;
  time(&c3_rawtime);
  c3_timeinfo = *localtime(&c3_rawtime);
  c3_timeinfo.tm_year += 1900;
  c3_timeinfo.tm_mon++;
  c3_timeinfoDouble.tm_min = (real_T)c3_timeinfo.tm_min;
  c3_timeinfoDouble.tm_sec = (real_T)c3_timeinfo.tm_sec;
  c3_timeinfoDouble.tm_hour = (real_T)c3_timeinfo.tm_hour;
  c3_timeinfoDouble.tm_mday = (real_T)c3_timeinfo.tm_mday;
  c3_timeinfoDouble.tm_mon = (real_T)c3_timeinfo.tm_mon;
  c3_timeinfoDouble.tm_year = (real_T)c3_timeinfo.tm_year;
  c3_x = c3_timeinfoDouble.tm_year / 4.0;
  c3_b_x = c3_x;
  c3_b_x = muDoubleScalarCeil(c3_b_x);
  c3_c_x = c3_timeinfoDouble.tm_year / 100.0;
  c3_d_x = c3_c_x;
  c3_d_x = muDoubleScalarCeil(c3_d_x);
  c3_e_x = c3_timeinfoDouble.tm_year / 400.0;
  c3_f_x = c3_e_x;
  c3_f_x = muDoubleScalarCeil(c3_f_x);
  c3_dDateNum = ((((365.0 * c3_timeinfoDouble.tm_year + c3_b_x) - c3_d_x) +
                  c3_f_x) + c3_cDaysMonthWise[(int32_T)c3_timeinfoDouble.tm_mon
                 - 1]) + c3_timeinfoDouble.tm_mday;
  if (c3_timeinfoDouble.tm_mon > 2.0) {
    c3_g_x = c3_timeinfoDouble.tm_year;
    c3_a = c3_g_x;
    c3_h_x = c3_a;
    c3_i_x = c3_h_x;
    if (c3_i_x == 0.0) {
      c3_r = 0.0;
    } else {
      c3_r = muDoubleScalarRem(c3_i_x, 4.0);
      c3_rEQ0 = (c3_r == 0.0);
      if (c3_rEQ0) {
        c3_r = 0.0;
      } else if (c3_i_x < 0.0) {
        c3_r += 4.0;
      }
    }

    c3_guard1 = false;
    if (c3_r == 0.0) {
      c3_j_x = c3_timeinfoDouble.tm_year;
      c3_b_a = c3_j_x;
      c3_k_x = c3_b_a;
      c3_m_x = c3_k_x;
      if (c3_m_x == 0.0) {
        c3_b_r = 0.0;
      } else {
        c3_b_r = muDoubleScalarRem(c3_m_x, 100.0);
        c3_b_rEQ0 = (c3_b_r == 0.0);
        if (c3_b_rEQ0) {
          c3_b_r = 0.0;
        } else if (c3_m_x < 0.0) {
          c3_b_r += 100.0;
        }
      }

      if (c3_b_r != 0.0) {
        c3_dDateNum++;
      } else {
        c3_guard1 = true;
      }
    } else {
      c3_guard1 = true;
    }

    if (c3_guard1) {
      c3_l_x = c3_timeinfoDouble.tm_year;
      c3_c_a = c3_l_x;
      c3_n_x = c3_c_a;
      c3_o_x = c3_n_x;
      if (c3_o_x == 0.0) {
        c3_c_r = 0.0;
      } else {
        c3_c_r = muDoubleScalarRem(c3_o_x, 400.0);
        c3_c_rEQ0 = (c3_c_r == 0.0);
        if (c3_c_rEQ0) {
          c3_c_r = 0.0;
        } else if (c3_o_x < 0.0) {
          c3_c_r += 400.0;
        }
      }

      if (c3_c_r == 0.0) {
        c3_dDateNum++;
      }
    }
  }

  c3_dDateNum += ((c3_timeinfoDouble.tm_hour * 3600.0 + c3_timeinfoDouble.tm_min
                   * 60.0) + c3_timeinfoDouble.tm_sec) / 86400.0;
  return c3_dDateNum;
}

static void c3_rand(SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance,
                    const emlrtStack *c3_sp, real_T c3_varargin_1,
                    c3_emxArray_real_T *c3_r)
{
  emlrtStack c3_b_st;
  emlrtStack c3_c_st;
  emlrtStack c3_st;
  real_T c3_b_k;
  real_T c3_b_varargin_1;
  real_T c3_c_varargin_1;
  real_T c3_d;
  real_T c3_d1;
  int32_T c3_i;
  int32_T c3_i1;
  int32_T c3_i2;
  int32_T c3_k;
  c3_st.prev = c3_sp;
  c3_st.tls = c3_sp->tls;
  c3_st.site = &c3_l_emlrtRSI;
  c3_b_st.prev = &c3_st;
  c3_b_st.tls = c3_st.tls;
  c3_c_st.prev = &c3_b_st;
  c3_c_st.tls = c3_b_st.tls;
  c3_b_varargin_1 = c3_varargin_1;
  if (!chartInstance->c3_method_not_empty) {
    chartInstance->c3_method = 7U;
    chartInstance->c3_method_not_empty = true;
  }

  c3_b_st.site = &c3_m_emlrtRSI;
  c3_c_varargin_1 = c3_b_varargin_1;
  if (!chartInstance->c3_state_not_empty) {
    for (c3_i = 0; c3_i < 625; c3_i++) {
      chartInstance->c3_state[c3_i] = c3_uv[c3_i];
    }

    chartInstance->c3_state_not_empty = true;
  }

  c3_i1 = c3_r->size[0];
  if (!(c3_c_varargin_1 >= 0.0)) {
    emlrtNonNegativeCheckR2012b(c3_c_varargin_1, &c3_c_emlrtDCI, &c3_b_st);
  }

  c3_r->size[0] = (int32_T)c3_c_varargin_1;
  c3_emxEnsureCapacity_real_T(chartInstance, &c3_b_st, c3_r, c3_i1,
    &c3_c_emlrtRTEI);
  c3_d = (real_T)c3_r->size[0];
  c3_i2 = (int32_T)c3_d - 1;
  for (c3_k = 0; c3_k <= c3_i2; c3_k++) {
    c3_b_k = (real_T)c3_k + 1.0;
    c3_c_st.site = &c3_n_emlrtRSI;
    c3_d1 = c3_b_eml_rand_mt19937ar(chartInstance, &c3_c_st,
      chartInstance->c3_state);
    c3_r->data[(int32_T)c3_b_k - 1] = c3_d1;
  }
}

static void c3_eml_rand_mt19937ar(SFc3_Building_Modeling_R2020aInstanceStruct
  *chartInstance, const emlrtStack *c3_sp, uint32_T c3_d_state[625], uint32_T
  c3_e_state[625], real_T *c3_r)
{
  int32_T c3_i;
  for (c3_i = 0; c3_i < 625; c3_i++) {
    c3_e_state[c3_i] = c3_d_state[c3_i];
  }

  *c3_r = c3_b_eml_rand_mt19937ar(chartInstance, c3_sp, c3_e_state);
}

static int32_T c3_combineVectorElements
  (SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance, const emlrtStack *
   c3_sp, c3_emxArray_boolean_T *c3_x)
{
  emlrtStack c3_b_st;
  emlrtStack c3_c_st;
  emlrtStack c3_st;
  int32_T c3_b;
  int32_T c3_b_b;
  int32_T c3_b_vlen;
  int32_T c3_c_b;
  int32_T c3_ix;
  int32_T c3_k;
  int32_T c3_vlen;
  int32_T c3_xoffset;
  int32_T c3_y;
  boolean_T c3_overflow;
  c3_st.prev = c3_sp;
  c3_st.tls = c3_sp->tls;
  c3_b_st.prev = &c3_st;
  c3_b_st.tls = c3_st.tls;
  c3_c_st.prev = &c3_b_st;
  c3_c_st.tls = c3_b_st.tls;
  c3_vlen = c3_x->size[0];
  if ((c3_x->size[0] == 0) || (c3_vlen == 0)) {
    c3_y = 0;
  } else {
    c3_st.site = &c3_r_emlrtRSI;
    c3_b_vlen = c3_vlen;
    c3_y = (int32_T)c3_x->data[0];
    c3_b_st.site = &c3_s_emlrtRSI;
    c3_b = c3_b_vlen;
    c3_b_b = c3_b;
    if (2 > c3_b_b) {
      c3_overflow = false;
    } else {
      c3_overflow = (c3_b_b > 2147483646);
    }

    if (c3_overflow) {
      c3_c_st.site = &c3_t_emlrtRSI;
      c3_check_forloop_overflow_error(chartInstance, &c3_c_st);
    }

    for (c3_k = 1; c3_k < c3_b_vlen; c3_k++) {
      c3_xoffset = c3_k + 1;
      c3_ix = c3_xoffset - 1;
      c3_c_b = (int32_T)c3_x->data[c3_ix];
      c3_y += (c3_c_b != 0);
    }
  }

  return c3_y;
}

static void c3_check_forloop_overflow_error
  (SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance, const emlrtStack *
   c3_sp)
{
  static char_T c3_cv[34] = { 'C', 'o', 'd', 'e', 'r', ':', 't', 'o', 'o', 'l',
    'b', 'o', 'x', ':', 'i', 'n', 't', '_', 'f', 'o', 'r', 'l', 'o', 'o', 'p',
    '_', 'o', 'v', 'e', 'r', 'f', 'l', 'o', 'w' };

  static char_T c3_cv1[5] = { 'i', 'n', 't', '3', '2' };

  const mxArray *c3_b_y = NULL;
  const mxArray *c3_c_y = NULL;
  const mxArray *c3_y = NULL;
  (void)chartInstance;
  c3_y = NULL;
  sf_mex_assign(&c3_y, sf_mex_create("y", c3_cv, 10, 0U, 1U, 0U, 2, 1, 34),
                false);
  c3_b_y = NULL;
  sf_mex_assign(&c3_b_y, sf_mex_create("y", c3_cv, 10, 0U, 1U, 0U, 2, 1, 34),
                false);
  c3_c_y = NULL;
  sf_mex_assign(&c3_c_y, sf_mex_create("y", c3_cv1, 10, 0U, 1U, 0U, 2, 1, 5),
                false);
  sf_mex_call(c3_sp, &c3_c_emlrtMCI, "error", 0U, 2U, 14, c3_y, 14, sf_mex_call
              (c3_sp, NULL, "getString", 1U, 1U, 14, sf_mex_call(c3_sp, NULL,
    "message", 1U, 2U, 14, c3_b_y, 14, c3_c_y)));
}

static real_T c3_emlrt_marshallIn(SFc3_Building_Modeling_R2020aInstanceStruct
  *chartInstance, const mxArray *c3_b_Occup, const char_T *c3_identifier)
{
  emlrtMsgIdentifier c3_thisId;
  real_T c3_y;
  c3_thisId.fIdentifier = (const char_T *)c3_identifier;
  c3_thisId.fParent = NULL;
  c3_thisId.bParentIsCell = false;
  c3_y = c3_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c3_b_Occup), &c3_thisId);
  sf_mex_destroy(&c3_b_Occup);
  return c3_y;
}

static real_T c3_b_emlrt_marshallIn(SFc3_Building_Modeling_R2020aInstanceStruct *
  chartInstance, const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId)
{
  real_T c3_d;
  real_T c3_y;
  (void)chartInstance;
  sf_mex_import(c3_parentId, sf_mex_dup(c3_u), &c3_d, 1, 0, 0U, 0, 0U, 0);
  c3_y = c3_d;
  sf_mex_destroy(&c3_u);
  return c3_y;
}

static real_T c3_c_emlrt_marshallIn(SFc3_Building_Modeling_R2020aInstanceStruct *
  chartInstance, const mxArray *c3_b_last_ind, const char_T *c3_identifier,
  boolean_T *c3_svPtr)
{
  emlrtMsgIdentifier c3_thisId;
  real_T c3_y;
  c3_thisId.fIdentifier = (const char_T *)c3_identifier;
  c3_thisId.fParent = NULL;
  c3_thisId.bParentIsCell = false;
  c3_y = c3_d_emlrt_marshallIn(chartInstance, sf_mex_dup(c3_b_last_ind),
    &c3_thisId, c3_svPtr);
  sf_mex_destroy(&c3_b_last_ind);
  return c3_y;
}

static real_T c3_d_emlrt_marshallIn(SFc3_Building_Modeling_R2020aInstanceStruct *
  chartInstance, const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId,
  boolean_T *c3_svPtr)
{
  real_T c3_d;
  real_T c3_y;
  (void)chartInstance;
  if (mxIsEmpty(c3_u)) {
    *c3_svPtr = false;
  } else {
    *c3_svPtr = true;
    sf_mex_import(c3_parentId, sf_mex_dup(c3_u), &c3_d, 1, 0, 0U, 0, 0U, 0);
    c3_y = c3_d;
  }

  sf_mex_destroy(&c3_u);
  return c3_y;
}

static uint32_T c3_e_emlrt_marshallIn
  (SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance, const mxArray
   *c3_b_method, const char_T *c3_identifier, boolean_T *c3_svPtr)
{
  emlrtMsgIdentifier c3_thisId;
  uint32_T c3_y;
  c3_thisId.fIdentifier = (const char_T *)c3_identifier;
  c3_thisId.fParent = NULL;
  c3_thisId.bParentIsCell = false;
  c3_y = c3_f_emlrt_marshallIn(chartInstance, sf_mex_dup(c3_b_method),
    &c3_thisId, c3_svPtr);
  sf_mex_destroy(&c3_b_method);
  return c3_y;
}

static uint32_T c3_f_emlrt_marshallIn
  (SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance, const mxArray
   *c3_u, const emlrtMsgIdentifier *c3_parentId, boolean_T *c3_svPtr)
{
  uint32_T c3_b_u;
  uint32_T c3_y;
  (void)chartInstance;
  if (mxIsEmpty(c3_u)) {
    *c3_svPtr = false;
  } else {
    *c3_svPtr = true;
    sf_mex_import(c3_parentId, sf_mex_dup(c3_u), &c3_b_u, 1, 7, 0U, 0, 0U, 0);
    c3_y = c3_b_u;
  }

  sf_mex_destroy(&c3_u);
  return c3_y;
}

static void c3_g_emlrt_marshallIn(SFc3_Building_Modeling_R2020aInstanceStruct
  *chartInstance, const mxArray *c3_d_state, const char_T *c3_identifier,
  boolean_T *c3_svPtr, uint32_T c3_y[625])
{
  emlrtMsgIdentifier c3_thisId;
  c3_thisId.fIdentifier = (const char_T *)c3_identifier;
  c3_thisId.fParent = NULL;
  c3_thisId.bParentIsCell = false;
  c3_h_emlrt_marshallIn(chartInstance, sf_mex_dup(c3_d_state), &c3_thisId,
                        c3_svPtr, c3_y);
  sf_mex_destroy(&c3_d_state);
}

static void c3_h_emlrt_marshallIn(SFc3_Building_Modeling_R2020aInstanceStruct
  *chartInstance, const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId,
  boolean_T *c3_svPtr, uint32_T c3_y[625])
{
  int32_T c3_i;
  uint32_T c3_b_uv[625];
  (void)chartInstance;
  if (mxIsEmpty(c3_u)) {
    *c3_svPtr = false;
  } else {
    *c3_svPtr = true;
    sf_mex_import(c3_parentId, sf_mex_dup(c3_u), c3_b_uv, 1, 7, 0U, 1, 0U, 1,
                  625);
    for (c3_i = 0; c3_i < 625; c3_i++) {
      c3_y[c3_i] = c3_b_uv[c3_i];
    }
  }

  sf_mex_destroy(&c3_u);
}

static void c3_i_emlrt_marshallIn(SFc3_Building_Modeling_R2020aInstanceStruct
  *chartInstance, const mxArray *c3_d_state, const char_T *c3_identifier,
  boolean_T *c3_svPtr, uint32_T c3_y[2])
{
  emlrtMsgIdentifier c3_thisId;
  c3_thisId.fIdentifier = (const char_T *)c3_identifier;
  c3_thisId.fParent = NULL;
  c3_thisId.bParentIsCell = false;
  c3_j_emlrt_marshallIn(chartInstance, sf_mex_dup(c3_d_state), &c3_thisId,
                        c3_svPtr, c3_y);
  sf_mex_destroy(&c3_d_state);
}

static void c3_j_emlrt_marshallIn(SFc3_Building_Modeling_R2020aInstanceStruct
  *chartInstance, const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId,
  boolean_T *c3_svPtr, uint32_T c3_y[2])
{
  int32_T c3_i;
  uint32_T c3_b_uv[2];
  (void)chartInstance;
  if (mxIsEmpty(c3_u)) {
    *c3_svPtr = false;
  } else {
    *c3_svPtr = true;
    sf_mex_import(c3_parentId, sf_mex_dup(c3_u), c3_b_uv, 1, 7, 0U, 1, 0U, 1, 2);
    for (c3_i = 0; c3_i < 2; c3_i++) {
      c3_y[c3_i] = c3_b_uv[c3_i];
    }
  }

  sf_mex_destroy(&c3_u);
}

static uint8_T c3_k_emlrt_marshallIn(SFc3_Building_Modeling_R2020aInstanceStruct
  *chartInstance, const mxArray *c3_b_is_active_c3_Building_Modeling_R2020a,
  const char_T *c3_identifier)
{
  emlrtMsgIdentifier c3_thisId;
  uint8_T c3_y;
  c3_thisId.fIdentifier = (const char_T *)c3_identifier;
  c3_thisId.fParent = NULL;
  c3_thisId.bParentIsCell = false;
  c3_y = c3_l_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c3_b_is_active_c3_Building_Modeling_R2020a), &c3_thisId);
  sf_mex_destroy(&c3_b_is_active_c3_Building_Modeling_R2020a);
  return c3_y;
}

static uint8_T c3_l_emlrt_marshallIn(SFc3_Building_Modeling_R2020aInstanceStruct
  *chartInstance, const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId)
{
  uint8_T c3_b_u;
  uint8_T c3_y;
  (void)chartInstance;
  sf_mex_import(c3_parentId, sf_mex_dup(c3_u), &c3_b_u, 1, 3, 0U, 0, 0U, 0);
  c3_y = c3_b_u;
  sf_mex_destroy(&c3_u);
  return c3_y;
}

static void c3_chart_data_browse_helper
  (SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance, int32_T
   c3_ssIdNumber, const mxArray **c3_mxData, uint8_T *c3_isValueTooBig)
{
  real_T c3_d;
  real_T c3_d1;
  *c3_mxData = NULL;
  *c3_mxData = NULL;
  *c3_isValueTooBig = 0U;
  switch (c3_ssIdNumber) {
   case 4U:
    c3_d = *chartInstance->c3_time;
    sf_mex_assign(c3_mxData, sf_mex_create("mxData", &c3_d, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 5U:
    c3_d1 = *chartInstance->c3_Occup;
    sf_mex_assign(c3_mxData, sf_mex_create("mxData", &c3_d1, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 6U:
    *c3_isValueTooBig = 1U;
    break;

   case 7U:
    sf_mex_assign(c3_mxData, sf_mex_create("mxData",
      &chartInstance->c3_persons_max, 0, 0U, 0U, 0U, 0), false);
    break;
  }
}

static real_T c3_b_eml_rand_mt19937ar
  (SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance, const emlrtStack *
   c3_sp, uint32_T c3_d_state[625])
{
  static char_T c3_cv[37] = { 'C', 'o', 'd', 'e', 'r', ':', 'M', 'A', 'T', 'L',
    'A', 'B', ':', 'r', 'a', 'n', 'd', '_', 'i', 'n', 'v', 'a', 'l', 'i', 'd',
    'T', 'w', 'i', 's', 't', 'e', 'r', 'S', 't', 'a', 't', 'e' };

  emlrtStack c3_st;
  const mxArray *c3_h_y = NULL;
  const mxArray *c3_i_y = NULL;
  real_T c3_b_j;
  real_T c3_b_r;
  real_T c3_c_kk;
  int32_T c3_a;
  int32_T c3_b_kk;
  int32_T c3_exitg1;
  int32_T c3_i;
  int32_T c3_j;
  int32_T c3_k;
  int32_T c3_kk;
  uint32_T c3_mt[625];
  uint32_T c3_u[2];
  uint32_T c3_b_y;
  uint32_T c3_c_y;
  uint32_T c3_d_y;
  uint32_T c3_e_y;
  uint32_T c3_f_y;
  uint32_T c3_g_y;
  uint32_T c3_mti;
  uint32_T c3_y;
  boolean_T c3_b_isvalid;
  boolean_T c3_exitg2;
  boolean_T c3_isvalid;
  (void)chartInstance;
  c3_st.prev = c3_sp;
  c3_st.tls = c3_sp->tls;
  c3_st.site = &c3_o_emlrtRSI;

  /* ========================= COPYRIGHT NOTICE ============================ */
  /*  This is a uniform (0,1) pseudorandom number generator based on:        */
  /*                                                                         */
  /*  A C-program for MT19937, with initialization improved 2002/1/26.       */
  /*  Coded by Takuji Nishimura and Makoto Matsumoto.                        */
  /*                                                                         */
  /*  Copyright (C) 1997 - 2002, Makoto Matsumoto and Takuji Nishimura,      */
  /*  All rights reserved.                                                   */
  /*                                                                         */
  /*  Redistribution and use in source and binary forms, with or without     */
  /*  modification, are permitted provided that the following conditions     */
  /*  are met:                                                               */
  /*                                                                         */
  /*    1. Redistributions of source code must retain the above copyright    */
  /*       notice, this list of conditions and the following disclaimer.     */
  /*                                                                         */
  /*    2. Redistributions in binary form must reproduce the above copyright */
  /*       notice, this list of conditions and the following disclaimer      */
  /*       in the documentation and/or other materials provided with the     */
  /*       distribution.                                                     */
  /*                                                                         */
  /*    3. The names of its contributors may not be used to endorse or       */
  /*       promote products derived from this software without specific      */
  /*       prior written permission.                                         */
  /*                                                                         */
  /*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
  /*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
  /*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
  /*  A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT  */
  /*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
  /*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
  /*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
  /*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
  /*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
  /*  (INCLUDING  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE */
  /*  OF THIS  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.  */
  /*                                                                         */
  /* =============================   END   ================================= */
  do {
    c3_exitg1 = 0;
    for (c3_j = 0; c3_j < 2; c3_j++) {
      c3_b_j = (real_T)c3_j + 1.0;
      c3_mti = c3_d_state[624] + 1U;
      if ((real_T)c3_mti >= 625.0) {
        for (c3_kk = 0; c3_kk < 227; c3_kk++) {
          c3_c_kk = (real_T)c3_kk + 1.0;
          c3_y = (c3_d_state[(int32_T)c3_c_kk - 1] & 2147483648U) | (c3_d_state
            [(int32_T)(c3_c_kk + 1.0) - 1] & 2147483647U);
          c3_b_y = c3_y;
          c3_d_y = c3_b_y;
          if ((c3_d_y & 1U) == 0U) {
            c3_d_y >>= 1U;
          } else {
            c3_d_y = c3_d_y >> 1U ^ 2567483615U;
          }

          c3_d_state[(int32_T)c3_c_kk - 1] = c3_d_state[(int32_T)(c3_c_kk +
            397.0) - 1] ^ c3_d_y;
        }

        for (c3_b_kk = 0; c3_b_kk < 396; c3_b_kk++) {
          c3_c_kk = (real_T)c3_b_kk + 228.0;
          c3_y = (c3_d_state[(int32_T)c3_c_kk - 1] & 2147483648U) | (c3_d_state
            [(int32_T)(c3_c_kk + 1.0) - 1] & 2147483647U);
          c3_f_y = c3_y;
          c3_g_y = c3_f_y;
          if ((c3_g_y & 1U) == 0U) {
            c3_g_y >>= 1U;
          } else {
            c3_g_y = c3_g_y >> 1U ^ 2567483615U;
          }

          c3_d_state[(int32_T)c3_c_kk - 1] = c3_d_state[(int32_T)((c3_c_kk + 1.0)
            - 228.0) - 1] ^ c3_g_y;
        }

        c3_y = (c3_d_state[623] & 2147483648U) | (c3_d_state[0] & 2147483647U);
        c3_c_y = c3_y;
        c3_e_y = c3_c_y;
        if ((c3_e_y & 1U) == 0U) {
          c3_e_y >>= 1U;
        } else {
          c3_e_y = c3_e_y >> 1U ^ 2567483615U;
        }

        c3_d_state[623] = c3_d_state[396] ^ c3_e_y;
        c3_mti = 1U;
      }

      c3_y = c3_d_state[(int32_T)c3_mti - 1];
      c3_d_state[624] = c3_mti;
      c3_y ^= c3_y >> 11U;
      c3_y ^= c3_y << 7U & 2636928640U;
      c3_y ^= c3_y << 15U & 4022730752U;
      c3_y ^= c3_y >> 18U;
      c3_u[(int32_T)c3_b_j - 1] = c3_y;
    }

    c3_u[0] >>= 5U;
    c3_u[1] >>= 6U;
    c3_b_r = 1.1102230246251565E-16 * ((real_T)c3_u[0] * 6.7108864E+7 + (real_T)
      c3_u[1]);
    if (c3_b_r == 0.0) {
      for (c3_i = 0; c3_i < 625; c3_i++) {
        c3_mt[c3_i] = c3_d_state[c3_i];
      }

      if (((real_T)c3_mt[624] >= 1.0) && ((real_T)c3_mt[624] < 625.0)) {
        c3_isvalid = true;
      } else {
        c3_isvalid = false;
      }

      c3_b_isvalid = c3_isvalid;
      if (c3_isvalid) {
        c3_b_isvalid = false;
        c3_k = 0;
        c3_exitg2 = false;
        while ((!c3_exitg2) && (c3_k + 1 < 625)) {
          if ((real_T)c3_mt[c3_k] == 0.0) {
            c3_a = c3_k + 1;
            c3_k = c3_a;
          } else {
            c3_b_isvalid = true;
            c3_exitg2 = true;
          }
        }
      }

      if (!c3_b_isvalid) {
        c3_h_y = NULL;
        sf_mex_assign(&c3_h_y, sf_mex_create("y", c3_cv, 10, 0U, 1U, 0U, 2, 1,
          37), false);
        c3_i_y = NULL;
        sf_mex_assign(&c3_i_y, sf_mex_create("y", c3_cv, 10, 0U, 1U, 0U, 2, 1,
          37), false);
        sf_mex_call(&c3_st, &c3_emlrtMCI, "error", 0U, 2U, 14, c3_h_y, 14,
                    sf_mex_call(&c3_st, NULL, "getString", 1U, 1U, 14,
          sf_mex_call(&c3_st, NULL, "message", 1U, 1U, 14, c3_i_y)));
      }
    } else {
      c3_exitg1 = 1;
    }
  } while (c3_exitg1 == 0);

  return c3_b_r;
}

static void c3_emxEnsureCapacity_boolean_T
  (SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance, const emlrtStack *
   c3_sp, c3_emxArray_boolean_T *c3_emxArray, int32_T c3_oldNumel, const
   emlrtRTEInfo *c3_srcLocation)
{
  int32_T c3_i;
  int32_T c3_newCapacity;
  int32_T c3_newNumel;
  void *c3_newData;
  (void)chartInstance;
  if (c3_oldNumel < 0) {
    c3_oldNumel = 0;
  }

  c3_newNumel = 1;
  for (c3_i = 0; c3_i < c3_emxArray->numDimensions; c3_i++) {
    c3_newNumel = (int32_T)emlrtSizeMulR2012b((size_t)(uint32_T)c3_newNumel,
      (size_t)(uint32_T)c3_emxArray->size[c3_i], c3_srcLocation, (void *)c3_sp);
  }

  if (c3_newNumel > c3_emxArray->allocatedSize) {
    c3_newCapacity = c3_emxArray->allocatedSize;
    if (c3_newCapacity < 16) {
      c3_newCapacity = 16;
    }

    while (c3_newCapacity < c3_newNumel) {
      if (c3_newCapacity > 1073741823) {
        c3_newCapacity = MAX_int32_T;
      } else {
        c3_newCapacity <<= 1;
      }
    }

    c3_newData = emlrtCallocMex((uint32_T)c3_newCapacity, sizeof(boolean_T));
    if (c3_newData == NULL) {
      emlrtHeapAllocationErrorR2012b(c3_srcLocation, (void *)c3_sp);
    }

    if (c3_emxArray->data != NULL) {
      memcpy(c3_newData, c3_emxArray->data, sizeof(boolean_T) * (uint32_T)
             c3_oldNumel);
      if (c3_emxArray->canFreeData) {
        emlrtFreeMex(c3_emxArray->data);
      }
    }

    c3_emxArray->data = (boolean_T *)c3_newData;
    c3_emxArray->allocatedSize = c3_newCapacity;
    c3_emxArray->canFreeData = true;
  }
}

static void c3_emxInit_boolean_T(SFc3_Building_Modeling_R2020aInstanceStruct
  *chartInstance, const emlrtStack *c3_sp, c3_emxArray_boolean_T **c3_pEmxArray,
  int32_T c3_numDimensions, const emlrtRTEInfo *c3_srcLocation)
{
  c3_emxArray_boolean_T *c3_emxArray;
  int32_T c3_i;
  (void)chartInstance;
  *c3_pEmxArray = (c3_emxArray_boolean_T *)emlrtMallocMex(sizeof
    (c3_emxArray_boolean_T));
  if ((void *)*c3_pEmxArray == NULL) {
    emlrtHeapAllocationErrorR2012b(c3_srcLocation, (void *)c3_sp);
  }

  c3_emxArray = *c3_pEmxArray;
  c3_emxArray->data = (boolean_T *)NULL;
  c3_emxArray->numDimensions = c3_numDimensions;
  c3_emxArray->size = (int32_T *)emlrtMallocMex(sizeof(int32_T) * (uint32_T)
    c3_numDimensions);
  if ((void *)c3_emxArray->size == NULL) {
    emlrtHeapAllocationErrorR2012b(c3_srcLocation, (void *)c3_sp);
  }

  c3_emxArray->allocatedSize = 0;
  c3_emxArray->canFreeData = true;
  for (c3_i = 0; c3_i < c3_numDimensions; c3_i++) {
    c3_emxArray->size[c3_i] = 0;
  }
}

static void c3_emxInit_real_T(SFc3_Building_Modeling_R2020aInstanceStruct
  *chartInstance, const emlrtStack *c3_sp, c3_emxArray_real_T **c3_pEmxArray,
  int32_T c3_numDimensions, const emlrtRTEInfo *c3_srcLocation)
{
  c3_emxArray_real_T *c3_emxArray;
  int32_T c3_i;
  (void)chartInstance;
  *c3_pEmxArray = (c3_emxArray_real_T *)emlrtMallocMex(sizeof(c3_emxArray_real_T));
  if ((void *)*c3_pEmxArray == NULL) {
    emlrtHeapAllocationErrorR2012b(c3_srcLocation, (void *)c3_sp);
  }

  c3_emxArray = *c3_pEmxArray;
  c3_emxArray->data = (real_T *)NULL;
  c3_emxArray->numDimensions = c3_numDimensions;
  c3_emxArray->size = (int32_T *)emlrtMallocMex(sizeof(int32_T) * (uint32_T)
    c3_numDimensions);
  if ((void *)c3_emxArray->size == NULL) {
    emlrtHeapAllocationErrorR2012b(c3_srcLocation, (void *)c3_sp);
  }

  c3_emxArray->allocatedSize = 0;
  c3_emxArray->canFreeData = true;
  for (c3_i = 0; c3_i < c3_numDimensions; c3_i++) {
    c3_emxArray->size[c3_i] = 0;
  }
}

static void c3_emxFree_boolean_T(SFc3_Building_Modeling_R2020aInstanceStruct
  *chartInstance, c3_emxArray_boolean_T **c3_pEmxArray)
{
  (void)chartInstance;
  if (*c3_pEmxArray != (c3_emxArray_boolean_T *)NULL) {
    if (((*c3_pEmxArray)->data != (boolean_T *)NULL) && (*c3_pEmxArray)
        ->canFreeData) {
      emlrtFreeMex((*c3_pEmxArray)->data);
    }

    emlrtFreeMex((*c3_pEmxArray)->size);
    emlrtFreeMex(*c3_pEmxArray);
    *c3_pEmxArray = (c3_emxArray_boolean_T *)NULL;
  }
}

static void c3_emxFree_real_T(SFc3_Building_Modeling_R2020aInstanceStruct
  *chartInstance, c3_emxArray_real_T **c3_pEmxArray)
{
  (void)chartInstance;
  if (*c3_pEmxArray != (c3_emxArray_real_T *)NULL) {
    if (((*c3_pEmxArray)->data != (real_T *)NULL) && (*c3_pEmxArray)
        ->canFreeData) {
      emlrtFreeMex((*c3_pEmxArray)->data);
    }

    emlrtFreeMex((*c3_pEmxArray)->size);
    emlrtFreeMex(*c3_pEmxArray);
    *c3_pEmxArray = (c3_emxArray_real_T *)NULL;
  }
}

static void c3_emxEnsureCapacity_real_T
  (SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance, const emlrtStack *
   c3_sp, c3_emxArray_real_T *c3_emxArray, int32_T c3_oldNumel, const
   emlrtRTEInfo *c3_srcLocation)
{
  int32_T c3_i;
  int32_T c3_newCapacity;
  int32_T c3_newNumel;
  void *c3_newData;
  (void)chartInstance;
  if (c3_oldNumel < 0) {
    c3_oldNumel = 0;
  }

  c3_newNumel = 1;
  for (c3_i = 0; c3_i < c3_emxArray->numDimensions; c3_i++) {
    c3_newNumel = (int32_T)emlrtSizeMulR2012b((size_t)(uint32_T)c3_newNumel,
      (size_t)(uint32_T)c3_emxArray->size[c3_i], c3_srcLocation, (void *)c3_sp);
  }

  if (c3_newNumel > c3_emxArray->allocatedSize) {
    c3_newCapacity = c3_emxArray->allocatedSize;
    if (c3_newCapacity < 16) {
      c3_newCapacity = 16;
    }

    while (c3_newCapacity < c3_newNumel) {
      if (c3_newCapacity > 1073741823) {
        c3_newCapacity = MAX_int32_T;
      } else {
        c3_newCapacity <<= 1;
      }
    }

    c3_newData = emlrtCallocMex((uint32_T)c3_newCapacity, sizeof(real_T));
    if (c3_newData == NULL) {
      emlrtHeapAllocationErrorR2012b(c3_srcLocation, (void *)c3_sp);
    }

    if (c3_emxArray->data != NULL) {
      memcpy(c3_newData, c3_emxArray->data, sizeof(real_T) * (uint32_T)
             c3_oldNumel);
      if (c3_emxArray->canFreeData) {
        emlrtFreeMex(c3_emxArray->data);
      }
    }

    c3_emxArray->data = (real_T *)c3_newData;
    c3_emxArray->allocatedSize = c3_newCapacity;
    c3_emxArray->canFreeData = true;
  }
}

static void init_dsm_address_info(SFc3_Building_Modeling_R2020aInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc3_Building_Modeling_R2020aInstanceStruct
  *chartInstance)
{
  chartInstance->c3_covrtInstance = (CovrtStateflowInstance *)
    sfrtGetCovrtInstance(chartInstance->S);
  chartInstance->c3_fEmlrtCtx = (void *)sfrtGetEmlrtCtx(chartInstance->S);
  chartInstance->c3_time = (real_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c3_Occup = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* SFunction Glue Code */
void sf_c3_Building_Modeling_R2020a_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(4024947935U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(3597958342U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(2010787420U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2995924056U);
}

mxArray *sf_c3_Building_Modeling_R2020a_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c3_Building_Modeling_R2020a_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "hiddenFallbackType", "hiddenFallbackReason", "incompatibleSymbol" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 5, infoFields);
  mxArray *fallbackType = mxCreateString("late");
  mxArray *fallbackReason = mxCreateString("ir_function_calls");
  mxArray *hiddenFallbackType = mxCreateString("");
  mxArray *hiddenFallbackReason = mxCreateString("");
  mxArray *incompatibleSymbol = mxCreateString("time");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], hiddenFallbackType);
  mxSetField(mxInfo, 0, infoFields[3], hiddenFallbackReason);
  mxSetField(mxInfo, 0, infoFields[4], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c3_Building_Modeling_R2020a_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

static const mxArray *sf_get_sim_state_info_c3_Building_Modeling_R2020a(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  mxArray *mxVarInfo = sf_mex_decode(
    "eNrtVkFv0zAUdqqu2jhMReIH9MJ1aleJbVzIVglRiYpqQqgHi+A4bmvJiavYmcotxx135Lifwk/"
    "gZ3DkyHF27UIxEZFotm5QS5bzXvScvO/73rOB1x8ANfbVvHwMQEOtu2rWgBk71vbUfGRX46//8L"
    "fUlB9nRPtFivuRWhMUL2yUzfvJmC/2PwY/928U7O+t7L9n/WZ8e7Fe/BPfja8XxO+sxDet/Qbjb"
    "GbxWeL09//R9NeL//37RXnsOnlomyEhA5pEOv4L+DPPnsNzzdo6hgEKlnj4JXnUnDyayyQ+Q9+s"
    "7yvB46Hw0nB40XZM5JRHJn7obZiXUW54ucz9Kuo1L8Fj6OCh7d5zOEz5JEVx6yVlRMDB6dvXp2f"
    "w/LB92EFQcs5CPockZpDREMZIMhTCFCXROEvgLKUXSBL9OtC+g/g+6MIvwWHfwUHbM5IKnghVso"
    "JGxPg2Xrc5Mvrw8X9Vt0X9NE0mihsq7wcv7wwv+eif4qVdwkv9F17qQBCyON9ebbqPfr0yfHy/q"
    "qSPzktw6Dn67K3dR5W6D+IHU59F9zYh1Tlg46eb1sN1ZPsmqUQPn0rwCB08wsrP1SDGk86z4/ZR"
    "sMB5nDErl61eKrkfW718uBu9YAcPfAt6kZ2Tk+4RSl3BbPWy7S8FehHTtIu5umNV219alehlVIL"
    "PUwcfbVMRICzpBQlwNzjLKIuoym7AI8L0g4apjVbuXze1aCJo"
    );
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c3_Building_Modeling_R2020a_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static const char* sf_get_instance_specialization(void)
{
  return "sTmfvdzkHMgNgKDkGyZIFkC";
}

static void sf_opaque_initialize_c3_Building_Modeling_R2020a(void
  *chartInstanceVar)
{
  initialize_params_c3_Building_Modeling_R2020a
    ((SFc3_Building_Modeling_R2020aInstanceStruct*) chartInstanceVar);
  initialize_c3_Building_Modeling_R2020a
    ((SFc3_Building_Modeling_R2020aInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_enable_c3_Building_Modeling_R2020a(void *chartInstanceVar)
{
  enable_c3_Building_Modeling_R2020a
    ((SFc3_Building_Modeling_R2020aInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_disable_c3_Building_Modeling_R2020a(void *chartInstanceVar)
{
  disable_c3_Building_Modeling_R2020a
    ((SFc3_Building_Modeling_R2020aInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_gateway_c3_Building_Modeling_R2020a(void *chartInstanceVar)
{
  sf_gateway_c3_Building_Modeling_R2020a
    ((SFc3_Building_Modeling_R2020aInstanceStruct*) chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c3_Building_Modeling_R2020a
  (SimStruct* S)
{
  return get_sim_state_c3_Building_Modeling_R2020a
    ((SFc3_Building_Modeling_R2020aInstanceStruct *)sf_get_chart_instance_ptr(S));/* raw sim ctx */
}

static void sf_opaque_set_sim_state_c3_Building_Modeling_R2020a(SimStruct* S,
  const mxArray *st)
{
  set_sim_state_c3_Building_Modeling_R2020a
    ((SFc3_Building_Modeling_R2020aInstanceStruct*)sf_get_chart_instance_ptr(S),
     st);
}

static void sf_opaque_cleanup_runtime_resources_c3_Building_Modeling_R2020a(void
  *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc3_Building_Modeling_R2020aInstanceStruct*)
                    chartInstanceVar)->S;
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_Building_Modeling_R2020a_optimization_info();
    }

    mdl_cleanup_runtime_resources_c3_Building_Modeling_R2020a
      ((SFc3_Building_Modeling_R2020aInstanceStruct*) chartInstanceVar);
    utFree(chartInstanceVar);
    if (ssGetUserData(S)!= NULL) {
      sf_free_ChartRunTimeInfo(S);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_mdl_start_c3_Building_Modeling_R2020a(void
  *chartInstanceVar)
{
  mdl_start_c3_Building_Modeling_R2020a
    ((SFc3_Building_Modeling_R2020aInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_mdl_terminate_c3_Building_Modeling_R2020a(void
  *chartInstanceVar)
{
  mdl_terminate_c3_Building_Modeling_R2020a
    ((SFc3_Building_Modeling_R2020aInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc3_Building_Modeling_R2020a
    ((SFc3_Building_Modeling_R2020aInstanceStruct*) chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c3_Building_Modeling_R2020a(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  sf_warn_if_symbolic_dimension_param_changed(S);
  if (sf_machine_global_initializer_called()) {
    initialize_params_c3_Building_Modeling_R2020a
      ((SFc3_Building_Modeling_R2020aInstanceStruct*)sf_get_chart_instance_ptr(S));
    initSimStructsc3_Building_Modeling_R2020a
      ((SFc3_Building_Modeling_R2020aInstanceStruct*)sf_get_chart_instance_ptr(S));
  }
}

const char* sf_c3_Building_Modeling_R2020a_get_post_codegen_info(void)
{
  int i;
  const char* encStrCodegen [25] = {
    "eNrtWc1vG0UUX1tp1CKIwofEoUiEQwWnKmkl2nIAx1/Uwm6s2gkRsmrGu8/ekWdnlplZJ4FLjr0",
    "g9QZHrvwJ3Dhy5IQQR7hw5IQ48mZ3k7ibXbup3SRts9JmM7vvvXnv937vzXjXytUaFh5LeP7ynm",
    "Ut4vUynnkrOi7F49zYGd1fsN6Px9+iEg+8JpHEU9bEgxMP7oMSLNBU8Brvi1Qxyvsggdso6wups",
    "6wp6gWM8mE14Laxpz53qe22XBEwp4i6xNngbA+t+YFuop0ylWDrKoCjXSmCgVtlZHDosdQ7JRfs",
    "oQq8SSEo0K3AN26pRsA09RlUdsGucaUJeqyOfGtpoqGkdzPDNJGq1oGg8HxGCU+N1iWqBT4CrGH",
    "Td/DvRqAxqKSY7RKpi+CSEag6HYY2BYekTarwQY9yooWkhFU8VjKKx31rMvSnIRxgEwBB34oSyN",
    "AXlOvs/LeqGGmFkx6DMvSCQba1FnwVmORvUdgBmYlbvyRGIMkANnjmpCEgld0wW4csOS6mqQdbR",
    "K7bmD8FTiZ7kTmqRTBP0EaNLDEIg6yptqQjhDfTWuDVDDOnlUzgRclW08RCa5URTMrCobWqzUuE",
    "MZUp1hZ+HUbAQqtloslkschqupxS1GkLBNjQO7saAk4x8bFYSXCHpqZrlBAI+849bCyPS9qB0sI",
    "rIXnL9frxx8fFalyD7BMb0rqAJFQBYhbCm23NocrkHgXRKx26lyYcMWSalKX6AS/vCDlETCY0ka",
    "MQTEYzBT01wFxiJWwqLJpJYiaX0+RsYrvgmAZDGTSwbFA2BRNlWts61t2I6r0yKFtSPyWrAVYdt",
    "qGKIdSeD5t8yMUOr0rhteIeH8ELgGwjklM+KGL/kntVnD0tFWY9W7WO1rNXn2A9y8d6VuL6wZid",
    "XIoda+yanPdKfvK8efwvF+sVxvReS8yzkNAzcst4/jX6Bq7+m//u6ps//vTHb8M/Z5n/9hsnW/+",
    "X4vE7B432sHBGx/hqZO+O+bWQYv/tMfvL8Vi1vf7I+Xp4tzG4N/isPPx074tadVgK7T18fbK/ry",
    "T8Pbi/Yjo+sizkp7RrTrwxMWMSRMt1iMeYv4tT8LgS34+Ofz6ZTf+tQlI/Da9LCbzMeMO2Az/B3",
    "6f3Y7kwm/7x+dPiuJyIw4wZUbpLuWP0f7VOxst8PDY6zKKWn1JfaXHkE3EsHwTxc6cQXR/MBY/n",
    "JS+LibyYsQfaFU6k38ydcV6296O8PNwvzKNe96fg0UzgYcaljzpNKQa4A1kJl6JOY71dXy927t9",
    "YvbFGOloI1hO7HfBYh9FexyOakV5HEu7g2t7xcYeI66N53DX3rnvngReFKTgsJXAwYx+kwlUdSx",
    "b3ehDdO/O63ScRPwr2S1W3af1U8gHmhurzkZetKC/72y9UXlan5GXhsbwsWAp/Tob7orPuo38/i",
    "vLx36O59NHdKTiUEvwszdxHkd3XveemPtP2beHvpFjfPWs+/ODEfRPmwofvp+DRS+DRm/u62vXs",
    "wdqHt1dvdUOc+wGL6XLBl7nsj2O+fHk6fLETeNjPgC967c6dm7eITBLmgi8X/SWFL8qVN22Be6z",
    "59peVufBlewo+1xL4XAvfr3eJeWsHXftmtxhQ5lCMLvwUYP4xMK2SxP7rad87nVTPutA7F3q5lz",
    "C+J3mPfNp61inrzRrfSd+nnzf5Sb/zrIT88jmOY9bvHM9a/vcTvqd4Nx5/fPjJseTiwpXy9Sl+X",
    "AfST3v6AvD0f0EVung=",
    ""
  };

  static char newstr [1745] = "";
  newstr[0] = '\0';
  for (i = 0; i < 25; i++) {
    strcat(newstr, encStrCodegen[i]);
  }

  return newstr;
}

static void mdlSetWorkWidths_c3_Building_Modeling_R2020a(SimStruct *S)
{
  const char* newstr = sf_c3_Building_Modeling_R2020a_get_post_codegen_info();
  sf_set_work_widths(S, newstr);
  ssSetChecksum0(S,(1702590179U));
  ssSetChecksum1(S,(2533553691U));
  ssSetChecksum2(S,(3114865435U));
  ssSetChecksum3(S,(3798718170U));
}

static void mdlRTW_c3_Building_Modeling_R2020a(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlSetupRuntimeResources_c3_Building_Modeling_R2020a(SimStruct *S)
{
  SFc3_Building_Modeling_R2020aInstanceStruct *chartInstance;
  chartInstance = (SFc3_Building_Modeling_R2020aInstanceStruct *)utMalloc(sizeof
    (SFc3_Building_Modeling_R2020aInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  memset(chartInstance, 0, sizeof(SFc3_Building_Modeling_R2020aInstanceStruct));
  chartInstance->chartInfo.chartInstance = chartInstance;
  if (ssGetSampleTime(S, 0) == CONTINUOUS_SAMPLE_TIME && ssGetOffsetTime(S, 0) ==
      0 && ssGetNumContStates(ssGetRootSS(S)) > 0 &&
      !supportsLegacyBehaviorForPersistentVarInContinuousTime(S)) {
    sf_error_out_about_continuous_sample_time_with_persistent_vars(S);
  }

  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway =
    sf_opaque_gateway_c3_Building_Modeling_R2020a;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c3_Building_Modeling_R2020a;
  chartInstance->chartInfo.mdlStart =
    sf_opaque_mdl_start_c3_Building_Modeling_R2020a;
  chartInstance->chartInfo.mdlTerminate =
    sf_opaque_mdl_terminate_c3_Building_Modeling_R2020a;
  chartInstance->chartInfo.mdlCleanupRuntimeResources =
    sf_opaque_cleanup_runtime_resources_c3_Building_Modeling_R2020a;
  chartInstance->chartInfo.enableChart =
    sf_opaque_enable_c3_Building_Modeling_R2020a;
  chartInstance->chartInfo.disableChart =
    sf_opaque_disable_c3_Building_Modeling_R2020a;
  chartInstance->chartInfo.getSimState =
    sf_opaque_get_sim_state_c3_Building_Modeling_R2020a;
  chartInstance->chartInfo.setSimState =
    sf_opaque_set_sim_state_c3_Building_Modeling_R2020a;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c3_Building_Modeling_R2020a;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c3_Building_Modeling_R2020a;
  chartInstance->chartInfo.mdlSetWorkWidths =
    mdlSetWorkWidths_c3_Building_Modeling_R2020a;
  chartInstance->chartInfo.callGetHoverDataForMsg = NULL;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartEventFcn = NULL;
  chartInstance->chartInfo.chartStateSetterFcn = NULL;
  chartInstance->chartInfo.chartStateGetterFcn = NULL;
  chartInstance->S = S;
  chartInstance->chartInfo.dispatchToExportedFcn = NULL;
  sf_init_ChartRunTimeInfo(S, &(chartInstance->chartInfo), false, 0,
    chartInstance->c3_JITStateAnimation,
    chartInstance->c3_JITTransitionAnimation);
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  mdl_setup_runtime_resources_c3_Building_Modeling_R2020a(chartInstance);
}

void c3_Building_Modeling_R2020a_method_dispatcher(SimStruct *S, int_T method,
  void *data)
{
  switch (method) {
   case SS_CALL_MDL_SETUP_RUNTIME_RESOURCES:
    mdlSetupRuntimeResources_c3_Building_Modeling_R2020a(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c3_Building_Modeling_R2020a(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c3_Building_Modeling_R2020a(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c3_Building_Modeling_R2020a_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}
