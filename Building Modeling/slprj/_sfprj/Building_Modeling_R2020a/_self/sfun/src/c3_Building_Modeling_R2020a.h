#ifndef __c3_Building_Modeling_R2020a_h__
#define __c3_Building_Modeling_R2020a_h__
#ifdef __has_include
#if __has_include(<time.h>)
#include <time.h>
#else
#error Cannot find header file <time.h> for imported type time_t.\
 Supply the missing header file or turn on Simulation Target -> Generate typedefs\
 for imported bus and enumeration types.
#endif

#else
#include <time.h>
#endif

/* Forward Declarations */
#ifndef typedef_c3_sxaDueAh1f53T9ESYg9Uc4E
#define typedef_c3_sxaDueAh1f53T9ESYg9Uc4E

typedef struct c3_tag_sxaDueAh1f53T9ESYg9Uc4E c3_sxaDueAh1f53T9ESYg9Uc4E;

#endif                                 /* typedef_c3_sxaDueAh1f53T9ESYg9Uc4E */

#ifndef typedef_c3_emxArray_boolean_T
#define typedef_c3_emxArray_boolean_T

typedef struct c3_emxArray_boolean_T c3_emxArray_boolean_T;

#endif                                 /* typedef_c3_emxArray_boolean_T */

#ifndef typedef_c3_emxArray_real_T
#define typedef_c3_emxArray_real_T

typedef struct c3_emxArray_real_T c3_emxArray_real_T;

#endif                                 /* typedef_c3_emxArray_real_T */

#ifndef typedef_c3_cell_0
#define typedef_c3_cell_0

typedef struct c3_tag_n3SDPft8LycMqfcEsfJVBB c3_cell_0;

#endif                                 /* typedef_c3_cell_0 */

#ifndef typedef_c3_s_wvzWMLKjXp7EJVnnMvwbTC
#define typedef_c3_s_wvzWMLKjXp7EJVnnMvwbTC

typedef struct c3_tag_wvzWMLKjXp7EJVnnMvwbTC c3_s_wvzWMLKjXp7EJVnnMvwbTC;

#endif                                 /* typedef_c3_s_wvzWMLKjXp7EJVnnMvwbTC */

/* Type Definitions */
#ifndef struct_c3_tag_sxaDueAh1f53T9ESYg9Uc4E
#define struct_c3_tag_sxaDueAh1f53T9ESYg9Uc4E

struct c3_tag_sxaDueAh1f53T9ESYg9Uc4E
{
  real_T tm_min;
  real_T tm_sec;
  real_T tm_hour;
  real_T tm_mday;
  real_T tm_mon;
  real_T tm_year;
};

#endif                                 /* struct_c3_tag_sxaDueAh1f53T9ESYg9Uc4E */

#ifndef typedef_c3_sxaDueAh1f53T9ESYg9Uc4E
#define typedef_c3_sxaDueAh1f53T9ESYg9Uc4E

typedef struct c3_tag_sxaDueAh1f53T9ESYg9Uc4E c3_sxaDueAh1f53T9ESYg9Uc4E;

#endif                                 /* typedef_c3_sxaDueAh1f53T9ESYg9Uc4E */

#ifndef struct_c3_emxArray_boolean_T
#define struct_c3_emxArray_boolean_T

struct c3_emxArray_boolean_T
{
  boolean_T *data;
  int32_T *size;
  int32_T allocatedSize;
  int32_T numDimensions;
  boolean_T canFreeData;
};

#endif                                 /* struct_c3_emxArray_boolean_T */

#ifndef typedef_c3_emxArray_boolean_T
#define typedef_c3_emxArray_boolean_T

typedef struct c3_emxArray_boolean_T c3_emxArray_boolean_T;

#endif                                 /* typedef_c3_emxArray_boolean_T */

#ifndef struct_c3_emxArray_real_T
#define struct_c3_emxArray_real_T

struct c3_emxArray_real_T
{
  real_T *data;
  int32_T *size;
  int32_T allocatedSize;
  int32_T numDimensions;
  boolean_T canFreeData;
};

#endif                                 /* struct_c3_emxArray_real_T */

#ifndef typedef_c3_emxArray_real_T
#define typedef_c3_emxArray_real_T

typedef struct c3_emxArray_real_T c3_emxArray_real_T;

#endif                                 /* typedef_c3_emxArray_real_T */

#ifndef struct_c3_tag_n3SDPft8LycMqfcEsfJVBB
#define struct_c3_tag_n3SDPft8LycMqfcEsfJVBB

struct c3_tag_n3SDPft8LycMqfcEsfJVBB
{
  char_T f1[6];
  char_T f2[6];
  char_T f3[7];
  char_T f4[7];
  char_T f5[6];
  char_T f6[7];
};

#endif                                 /* struct_c3_tag_n3SDPft8LycMqfcEsfJVBB */

#ifndef typedef_c3_cell_0
#define typedef_c3_cell_0

typedef struct c3_tag_n3SDPft8LycMqfcEsfJVBB c3_cell_0;

#endif                                 /* typedef_c3_cell_0 */

#ifndef struct_c3_tag_wvzWMLKjXp7EJVnnMvwbTC
#define struct_c3_tag_wvzWMLKjXp7EJVnnMvwbTC

struct c3_tag_wvzWMLKjXp7EJVnnMvwbTC
{
  c3_cell_0 _data;
};

#endif                                 /* struct_c3_tag_wvzWMLKjXp7EJVnnMvwbTC */

#ifndef typedef_c3_s_wvzWMLKjXp7EJVnnMvwbTC
#define typedef_c3_s_wvzWMLKjXp7EJVnnMvwbTC

typedef struct c3_tag_wvzWMLKjXp7EJVnnMvwbTC c3_s_wvzWMLKjXp7EJVnnMvwbTC;

#endif                                 /* typedef_c3_s_wvzWMLKjXp7EJVnnMvwbTC */

#ifndef typedef_SFc3_Building_Modeling_R2020aInstanceStruct
#define typedef_SFc3_Building_Modeling_R2020aInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  int32_T c3_sfEvent;
  boolean_T c3_doneDoubleBufferReInit;
  uint8_T c3_is_active_c3_Building_Modeling_R2020a;
  uint8_T c3_JITStateAnimation[1];
  real_T c3_distribution_data[20160];
  real_T c3_persons_max;
  uint8_T c3_JITTransitionAnimation[1];
  int32_T c3_IsDebuggerActive;
  int32_T c3_IsSequenceViewerPresent;
  int32_T c3_SequenceViewerOptimization;
  int32_T c3_IsHeatMapPresent;
  void *c3_RuntimeVar;
  real_T c3_rng_init;
  boolean_T c3_rng_init_not_empty;
  real_T c3_last_ind;
  boolean_T c3_last_ind_not_empty;
  real_T c3_persons_inside;
  boolean_T c3_persons_inside_not_empty;
  uint32_T c3_seed;
  boolean_T c3_seed_not_empty;
  uint32_T c3_method;
  boolean_T c3_method_not_empty;
  uint32_T c3_state[625];
  boolean_T c3_state_not_empty;
  uint32_T c3_b_state[2];
  boolean_T c3_b_state_not_empty;
  uint32_T c3_c_state;
  boolean_T c3_c_state_not_empty;
  uint32_T c3_mlFcnLineNumber;
  void *c3_fcnDataPtrs[5];
  char_T *c3_dataNames[5];
  uint32_T c3_numFcnVars;
  uint32_T c3_ssIds[5];
  uint32_T c3_statuses[5];
  void *c3_outMexFcns[5];
  void *c3_inMexFcns[5];
  CovrtStateflowInstance *c3_covrtInstance;
  void *c3_fEmlrtCtx;
  real_T *c3_time;
  real_T *c3_Occup;
} SFc3_Building_Modeling_R2020aInstanceStruct;

#endif                                 /* typedef_SFc3_Building_Modeling_R2020aInstanceStruct */

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray
  *sf_c3_Building_Modeling_R2020a_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c3_Building_Modeling_R2020a_get_check_sum(mxArray *plhs[]);
extern void c3_Building_Modeling_R2020a_method_dispatcher(SimStruct *S, int_T
  method, void *data);

#endif
